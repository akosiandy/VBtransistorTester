﻿Imports System.Data.SQLite

Public Class ntsadjust
    Dim adapter As New SQLiteDataAdapter
    Dim dataset As New DataTable
    Dim source As New BindingSource
    Dim kgucount
    Dim multiplied
    Dim multipler As Integer
    Dim datatorun
    Dim units
    Private Sub ntsadjust_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        transistorselect()
    End Sub

    Private Sub transistorselect()
        Dim mysqlcongraph = New SQLiteConnection
        mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
        Try
            mysqlcongraph.Open()
            Dim querygraph As String = "Select DISTINCT Testcode, Transistorname,Dtime FROM transdata where iskgu='NO' "
            Dim adapter As New SQLiteDataAdapter
            Dim Command = New SQLiteCommand(querygraph, mysqlcongraph)
            Dim reader = Command.ExecuteReader()
            Command = New SQLiteCommand(querygraph, mysqlcongraph)
            adapter.SelectCommand = Command
            Dim index As Integer = 0
            While reader.Read
                cbtrans.Items.Add(reader("Testcode") & " | " & reader("Transistorname") & " | " & reader("Dtime"))
            End While
            mysqlcongraph.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        End Try

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim seted() As String = Split(cbtrans.Text, " | ", 3)
        Dim testcode = seted(0)
        Dim transtest = seted(1)


        Select Case cbshowme.Text
            Case "Show ALL"
                rundatagrid("Select Trial,rb,rc,re,vc as VC,vre as VRE, vrc as vrc, vce as VCE,vb as VB,ib as IB,ic as IC,ie as IE,B as Beta,Testcode, Transistorname as 'Transistor in test' FROM transdata where testcode ='" & testcode & "'")
            Case "Show Voltage Only"
                rundatagrid("Select Trial,rb,rc,re,vc as VC,vre as VRE, vce as VCE,vb as VB,B as Beta,Testcode, Transistorname as 'Transistor in test' FROM transdata where testcode ='" & testcode & "'")
            Case "Show Current Only"
                rundatagrid("Select Trial,rb,rc,re,ib as IB,ic as IC,ie as IE,B as Beta,Testcode, Transistorname as 'Transistor in test' FROM transdata where testcode ='" & testcode & "'")
            Case "VC"
                rundatagrid("Select Trial,rb,rc,re,vc as VC, Transistorname as 'Transistor in test' FROM transdata where testcode ='" & testcode & "'")
            Case "VB"
                rundatagrid("Select Trial,rb,rc,re,vb as VB, Transistorname as 'Transistor in test' FROM transdata where testcode ='" & testcode & "'")
            Case "VCE"
                rundatagrid("Select Trial,rb,rc,re, vce as VCE, Transistorname as 'Transistor in test' FROM transdata where testcode ='" & testcode & "'")

            Case "VRC"
                rundatagrid("Select Trial,rb,rc,re,vrc as VRC,Transistorname as 'Transistor in test' FROM transdata where testcode ='" & testcode & "'")

            Case "VRE"
                rundatagrid("Select Trial,rb,rc,re,vre as VRE, Transistorname as 'Transistor in test' FROM transdata where testcode ='" & testcode & "'")

            Case "IB"
                rundatagrid("Select Trial,rb,rc,re,ib as IB, Transistorname as 'Transistor in test' FROM transdata where testcode ='" & testcode & "'")

            Case "IC"
                rundatagrid("Select Trial,rb,rc,re,ic as IC, Transistorname as 'Transistor in test' FROM transdata where testcode ='" & testcode & "'")

            Case "IE"
                rundatagrid("Select Trial,rb,rc,re,ie as IE, Transistorname as 'Transistor in test' FROM transdata where testcode ='" & testcode & "'")

            Case "BETA"
                rundatagrid("Select Trial,rb,rc,B as Beta, Transistorname as 'Transistor in test' FROM transdata where testcode ='" & testcode & "'")
        End Select

        Select Case cbchart.Text
            Case "Beta"
                multipler = 1
                Name = "Beta"
                datatorun = "B"
                units = ""
            Case "VC"
                multipler = 1
                Name = "Voltage In Collector"
                datatorun = "vc"
                units = "in Volts"
            Case "VRE"
                multipler = 1000
                Name = "Voltage In Emitter"
                datatorun = "vre"
                units = "in milliVolts"
            Case "VCE"
                multipler = 1000
                Name = "Voltage In Collector Emitter"
                datatorun = "vce"
                units = "in milliVolts"
            Case "VRC"
                multipler = 1
                Name = "Voltage In R Collector"
                datatorun = "vrc"
                units = "in Volts"
            Case "VB"
                multipler = 1
                Name = "Voltage In Base"
                datatorun = "vb"
                units = "in Volts"
            Case "IB"
                multipler = 1000000
                Name = "Current In Base"
                datatorun = "ib"
                units = "in microAmps"
            Case "I"
                multipler = 1000
                Name = "Current In Collector"
                datatorun = "ic"
                units = "in milliAmps"
            Case "IE"
                multipler = 1000
                Name = "Current In Emitter"
                datatorun = "ie"
                units = "in milliAmps"
        End Select
        Chart2.Titles("TopName").Text = Name
        Chart2.ChartAreas(0).AxisY.Title = units
        runchart(datatorun, testcode)
    End Sub
    Private Sub runchart(ByVal query As String, ByVal testcode As String)
        Dim chartkgu
        Dim count
        Chart2.Series(0).Points.Clear()
        Chart2.Series(1).Points.Clear()
        Dim mysqlcongraph = New SQLiteConnection
        mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
        Try
            mysqlcongraph.Open()
            Dim querygraph As String = "Select Trial , " & query & " FROM transdata Where iskgu = 'NO' AND Testcode='" & testcode & "'" ' Where Transistorname=" + Form1.kguintest
            Dim Command = New SQLiteCommand(querygraph, mysqlcongraph)
            Dim reader = Command.ExecuteReader()


            While reader.Read
                multiplied = reader(query)
                multiplied = multiplied * multipler
                Chart2.Series(1).Points.AddXY(reader("Trial").ToString, multiplied)
                kgucount = kgucount + 1
            End While
            mysqlcongraph.Close()

            mysqlcongraph.Open()
            Dim querygraph1 As String = "SELECT min(" & query & ") , max(" & query & ") FROM transdata Where iskgu = 'NO' AND Testcode='" & testcode & "'"
            Command = New SQLiteCommand(querygraph1, mysqlcongraph)
            reader = Command.ExecuteReader

            While reader.Read
                multiplied = reader("min(" & query & ")")
                multiplied = multiplied * multipler
                multiplied = Math.Round(multiplied, 0)
                Chart2.ChartAreas("data1").AxisY.Minimum = multiplied - 20
                Chart2.ChartAreas("data1").AxisY2.Minimum = multiplied - 20

                multiplied = reader("max(" & query & ")")
                multiplied = multiplied * multipler
                multiplied = Math.Round(multiplied, 0)
                Chart2.ChartAreas("data1").AxisY.Maximum = multiplied + 20
                Chart2.ChartAreas("data1").AxisY2.Maximum = multiplied + 20

            End While


            mysqlcongraph.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        End Try

        mysqlcongraph = New SQLiteConnection
        mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
        Try
            mysqlcongraph.Open()
            Dim querygraph As String = "Select " & query & " FROM transdata Where iskgu = 'YES' AND Transistorname='" & testcode & "'"
            Console.WriteLine(querygraph)
            Dim Command = New SQLiteCommand(querygraph, mysqlcongraph)
            Dim reader = Command.ExecuteReader
            multiplied = 0
            While reader.Read
                multiplied = reader(query)
                multiplied = multiplied * multipler
                chartkgu = multiplied
            End While
            While (kgucount >= count And multiplied <> 0)
                Chart2.Series(0).Points.AddY(chartkgu.ToString)
                count = count + 1
            End While
            mysqlcongraph.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        End Try
    End Sub
    Private Sub rundatagrid(ByVal querygraph As String)
        dataset.Clear()
        dataset.Columns.Clear()
        Dim mysqlcongraph = New SQLiteConnection
        mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
        Try
            mysqlcongraph.Open()
            'querygraph As String = "Select * FROM transdata "
            Dim Command = New SQLiteCommand(querygraph, mysqlcongraph)
            Dim reader = Command.ExecuteReader()

            Command = New SQLiteCommand(querygraph, mysqlcongraph)
            adapter.SelectCommand = Command
            adapter.Fill(dataset)
            source.DataSource = dataset
            cbgraph.DataSource = source
            cbgraph.AutoResizeColumns()
            While reader.Read
                Dim increment As Integer = increment + 1
                cbgraph.Columns(reader.GetName(increment)).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End While
            adapter.Update(dataset)
            mysqlcongraph.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        End Try
        cbgraph.Refresh()
    End Sub

    Private Sub cbtrans_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbtrans.SelectedIndexChanged
        cbchart.Enabled = True
        cbshowme.Enabled = True
        Button1.Enabled = True
    End Sub

    Private Sub Chart2_Click(sender As Object, e As EventArgs) Handles Chart2.Click

    End Sub
End Class