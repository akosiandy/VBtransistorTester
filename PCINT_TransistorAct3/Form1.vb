﻿Imports System.Data.SQLite
Public Class Form1


    Dim mysqlcon As SQLiteConnection
    Dim mysqlcongraph As SQLiteConnection
    Dim command As SQLiteCommand
    Dim reader As SQLiteDataReader
    Dim source As New BindingSource

    Dim vc
    Dim vre
    Dim a1 As Integer
    Dim rb As Integer = 100000
    Dim rc As Integer = 500
    Dim re As Integer = 50
    Dim aip As Boolean = False
    Dim trial As Integer = 1
    Dim curtrial As Integer = 0
    Dim nexttrans As Boolean = True

    Dim vce As Double
    Dim vrc As Double
    Dim vb As Double
    Dim vrb As Double
    Dim ib As Double
    Dim ic As Double
    Dim ie As Double
    Dim b As Double
    Dim timestarted As DateTime
    Dim autotestcode As Integer = 0
    Public kguintest As String
    Private Sub btnStart_Click(sender As Object, e As EventArgs)
        Timer1.Start()

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Dim incoming As String
        Try
            incoming = SerialPort1.ReadExisting()
            If incoming Is Nothing Then

            Else
                Dim seted() As String = Split(incoming, vbCrLf, 4)
                a1 = Integer.Parse(seted(1))
                Dim vccinput As Double = (Integer.Parse(seted(0)) / 1024) * 5
                Dim vcinput As Double = (Integer.Parse(seted(1)) / 1024) * 5
                Dim vbinput As Double = (Integer.Parse(seted(2)) / 1024) * 5
                Dim vreinput As Double = (Integer.Parse(seted(3)) / 1024) * 5
                Try
                    vc = Math.Round(vcinput, 3)
                    vre = Math.Round(vreinput, 3)
                    vb = Math.Round(vbinput, 3)
                Catch

                End Try

                Dim vcc As Double = Math.Round(vccinput, 3)
                    If (vc < 1) Then
                        TextBox1.Text = vc * 1000
                        lblunitvc.Text = "mV"
                    Else
                        TextBox1.Text = vc
                        lblunitvc.Text = "V"
                    End If
                    If (vre < 1) Then
                        TextBox2.Text = vre * 1000
                        lblunitvre.Text = "mV"
                    Else
                        TextBox2.Text = vre
                        lblunitvre.Text = "V"
                    End If
                    vce = vc - vre
                    vrc = vcc - vc
                    vrb = vcc - vb
                    ib = vrb / rb
                    ic = vrc / rc
                    ie = vre / re
                    b = ic / ib



                    If (vce < 1) Then
                        txtvce.Text = vce * 1000
                        lblvce.Text = "mV"
                    Else
                        txtvce.Text = vce
                        lblvce.Text = "V"
                    End If

                    If (vrc < 1) Then
                        txtvrc.Text = vrc * 1000
                        lblvrc.Text = "mV"
                    Else
                        txtvrc.Text = vrc
                        lblvrc.Text = "V"
                    End If

                    If (vb < 1) Then
                        txtvb.Text = vb * 1000
                        lblvb.Text = "mV"
                    Else
                        txtvb.Text = vb
                        lblvb.Text = "V"
                    End If


                    txtvcc.Text = vcc
                    txtib.Text = ib * 1000000
                    txtic.Text = ic * 1000
                    txtie.Text = Math.Round(ie * 1000, 3)
                    txtb.Text = Math.Round(b, 3)
                    If (vre = 0) Then
                        txtvce.Text = 0
                        txtvrc.Text = 0
                        txtvb.Text = 0
                        txtib.Text = 0
                        txtic.Text = 0
                        txtie.Text = 0
                        txtb.Text = 0
                    End If
                    If (aip = True) Then
                        If (seted(1) <> "1023" And vre <> 0) Then
                            If (nexttrans = True) Then
                                WritetoDB(vc, vre)
                                nexttrans = False
                            End If
                        Else
                            nexttrans = True
                            lblstatus.Text = "Insert Transistor" + curtrial.ToString + "/" + trial.ToString
                        End If
                    End If
                End If
        Catch ex As Exception
            lblstatus.Text = ex.Message
        End Try
    End Sub

    Private Sub WritetoDB(vcdb As Double, vredb As Double)
        If (curtrial <= trial) Then

            lblstatus.Text = "Testing"
            mysqlcongraph = New SQLiteConnection
            mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
            Try
                mysqlcongraph.Open()
                Dim querygraph As String = "INSERT INTO transdata (rb, rc, re, Transistorname, vc, vre,vce, vrc, vb, ib, ic, ie, B, iskgu,trial,Testcode,Dtime) VALUES ('" & cbRB.Text & "','" & cbRC.Text & "','" & cbRE.Text & "','" & ComboBox1.Text & "','" & vcdb & "','" & vredb & "','" & vce & "','" & vrc & "','" & vb & "','" & ib & "','" & ic & "','" & ie & "','" & b & "','" & "NO" & "','" & curtrial + 1 & "','" & txtTestcode.Text & "','" & timestarted & "')"
                Console.WriteLine(querygraph)
                command = New SQLiteCommand(querygraph, mysqlcongraph)
                reader = command.ExecuteReader
                mysqlcongraph.Close()
                nexttrans = False
            Catch ex As Exception
                Console.Write(ex.Message)
            End Try
            curtrial += 1
            pb1.Value = curtrial
            lblstatus.Text = "Done Testing Pull the Current Transistor"
            Beep()

            If (curtrial = trial) Then
                pb1.Value = 0
                aip = False
                lblstatus.Text = "Testing Complete with " + trial.ToString + " sets of Trial"
                Beep()
                Beep()

                trial = 0
                curtrial = 0
                Dim response = MsgBox("Do you want to display the Graph, If no Set another parameter again", MsgBoxStyle.YesNo, "Testing Done")
                If response = MsgBoxResult.Yes Then
                    Mparent.runallformsgbox("showall")
                End If
                gbComputed.Enabled = True
                gbMeasured.Enabled = True
                gbParam.Enabled = True
            End If
        End If
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles btnstartform1.Click
        Dim sendstring As String
        If (cbRB.Text = "100k Ohms") Then
            sendstring = "0"
            rb = 100000
        Else
            sendstring = "1"
            rb = 200000
        End If

        If (cbRC.Text = "500 Ohms") Then
            sendstring += "0"
            rc = 500
        Else
            sendstring += "1"
            rc = 1000
        End If

        If (cbRE.Text = "50 Ohms") Then
            sendstring += "0"
            re = 50
        Else
            sendstring += "1"
            re = 30
        End If
        SerialPort1.Write(sendstring)
        Timer1.Stop()
        Timer1.Start()
        Mparent.Activetestcode = txtTestcode.Text
        Mparent.Activekgu = ComboBox1.Text
        trial = txttrial.Text
        aip = True
        pb1.Maximum = trial
        nexttrans = True
        If (aip = True) Then
            gbComputed.Enabled = False
            gbMeasured.Enabled = False
            gbParam.Enabled = False
        End If
        timestarted = DateTime.Now.ToString("dd-MMMM-yyyy  hh:mm:ss")
    End Sub
    Public Sub setkgutodb(transname As String)
        System.Threading.Thread.Sleep(Mparent.delayvalue)

        If (a1 <> "1023" And vre <> 0) Then
            mysqlcongraph = New SQLiteConnection
            mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
            Try
                mysqlcongraph.Open()
                Dim querygraph As String = "INSERT INTO transdata (rb, rc, re, Transistorname, vc, vre,vce, vrc, vb, ib, ic, ie, B, iskgu) VALUES ('" & cbRB.Text & "','" & cbRC.Text & "','" & cbRE.Text & "','" & transname & "','" & vc & "','" & vre & "','" & vce & "','" & vrc & "','" & vb & "','" & ib & "','" & ic & "','" & ie & "','" & b & "','" & "YES" & "')"
                command = New SQLiteCommand(querygraph, mysqlcongraph)
                reader = command.ExecuteReader
                mysqlcongraph.Close()
                kguintest = transname
                MsgBox("Succesfully Added the Transistor " + transname + " Into KGU")
            Catch ex As Exception
                Console.Write(ex.Message)
            End Try
        Else
            MsgBox("Insert the KGU then click Set KGU again")
        End If
    End Sub
    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        kguintest = ComboBox1.Text
    End Sub
    Private Sub ME_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        SerialPort1.Close()
        Mparent.TestModeToolStripMenuItem.Checked = False
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If (Mparent.ActiveComport = "") Then ' run only once
            Me.Text = "Select a Com Port Under the tools menu"
            Me.Enabled = False
            ' add kgu as dynamics
        End If
        refreshKGU()
        'inittestcode()
        btnstartform1.Enabled = False
    End Sub
    Public Sub refreshKGU()
        Dim mysqlcongraph = New SQLiteConnection
        mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
        Try
            mysqlcongraph.Open()
            Dim querygraph As String = "Select DISTINCT Transistorname FROM KGUdataLIST "
            Dim Command = New SQLiteCommand(querygraph, mysqlcongraph)
            Dim reader = Command.ExecuteReader()

            Command = New SQLiteCommand(querygraph, mysqlcongraph)
            SetKGUToolStripMenuItem1.DropDownItems.Clear()
            ComboBox1.Items.Clear()
            While reader.Read
                Me.SetKGUToolStripMenuItem1.DropDownItems.Add(reader("Transistorname"), Nothing, AddressOf kguset)
                ComboBox1.Items.Add(reader("Transistorname"))
            End While
            mysqlcongraph.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        End Try
    End Sub
    Private Sub kguset(sender As Object, e As EventArgs)
        Dim kgu = DirectCast(sender, ToolStripMenuItem)
        kgu.Checked = True
        setkgutodb(kgu.Text)
    End Sub

    Private Sub SetKGUToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles SetKGUToolStripMenuItem1.Click
        refreshKGU()
    End Sub

    Private Sub ConnectAndStartToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConnectAndStartToolStripMenuItem.Click

        SerialPort1.BaudRate = 9600
        SerialPort1.DataBits = 8
        SerialPort1.StopBits = IO.Ports.StopBits.One
        SerialPort1.Parity = IO.Ports.Parity.None

        Try
            SerialPort1.PortName = Mparent.ActiveComport
            SerialPort1.Open()
            lblstatus.Text = "Connected " + SerialPort1.PortName
        Catch ex As Exception
            lblstatus.Text = "Error Occured While Connecting Error: " + ex.Message
        End Try
        btnstartform1.Enabled = True
        Timer1.Enabled = True
    End Sub
End Class





