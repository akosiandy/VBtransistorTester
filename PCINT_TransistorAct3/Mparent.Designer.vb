﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Mparent
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Mparent))
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.FileMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddKGUsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveCustomKGUsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveSavedKGUToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveTestsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.TestModeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GraphToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.InputSpecificTestcodeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PreviousTestToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.NTSAdjustableViewsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolsMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.comtoolportmenuitem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TestDelaySettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataBaseSettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CreateStandAloneDatabaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NTSDataDumpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WindowsMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.CascadeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TileVerticalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TileHorizontalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CloseAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArduinoDataStringsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DateandtimeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.parentPortcheck = New System.IO.Ports.SerialPort(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.MenuStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip
        '
        Me.MenuStrip.AllowMerge = False
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileMenu, Me.ViewMenu, Me.ShowToolStripMenuItem, Me.ToolsMenu, Me.WindowsMenu, Me.HelpMenu, Me.DateandtimeToolStripMenuItem})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.MdiWindowListItem = Me.WindowsMenu
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Size = New System.Drawing.Size(632, 24)
        Me.MenuStrip.TabIndex = 5
        Me.MenuStrip.Text = "MenuStrip"
        '
        'FileMenu
        '
        Me.FileMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddKGUsToolStripMenuItem, Me.RemoveCustomKGUsToolStripMenuItem, Me.RemoveSavedKGUToolStripMenuItem, Me.RemoveTestsToolStripMenuItem, Me.PrintToolStripMenuItem})
        Me.FileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder
        Me.FileMenu.Name = "FileMenu"
        Me.FileMenu.Size = New System.Drawing.Size(37, 20)
        Me.FileMenu.Text = "&File"
        '
        'AddKGUsToolStripMenuItem
        '
        Me.AddKGUsToolStripMenuItem.Name = "AddKGUsToolStripMenuItem"
        Me.AddKGUsToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.AddKGUsToolStripMenuItem.Text = "Add Custom KGUs"
        '
        'RemoveCustomKGUsToolStripMenuItem
        '
        Me.RemoveCustomKGUsToolStripMenuItem.Name = "RemoveCustomKGUsToolStripMenuItem"
        Me.RemoveCustomKGUsToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.RemoveCustomKGUsToolStripMenuItem.Text = "Remove Custom KGUs"
        '
        'RemoveSavedKGUToolStripMenuItem
        '
        Me.RemoveSavedKGUToolStripMenuItem.Name = "RemoveSavedKGUToolStripMenuItem"
        Me.RemoveSavedKGUToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.RemoveSavedKGUToolStripMenuItem.Text = "Remove Saved KGU DATA"
        '
        'RemoveTestsToolStripMenuItem
        '
        Me.RemoveTestsToolStripMenuItem.Name = "RemoveTestsToolStripMenuItem"
        Me.RemoveTestsToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.RemoveTestsToolStripMenuItem.Text = "Remove Tests"
        '
        'PrintToolStripMenuItem
        '
        Me.PrintToolStripMenuItem.Image = CType(resources.GetObject("PrintToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PrintToolStripMenuItem.Name = "PrintToolStripMenuItem"
        Me.PrintToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.PrintToolStripMenuItem.Text = "Print"
        '
        'ViewMenu
        '
        Me.ViewMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TestModeToolStripMenuItem})
        Me.ViewMenu.Enabled = False
        Me.ViewMenu.Name = "ViewMenu"
        Me.ViewMenu.Size = New System.Drawing.Size(44, 20)
        Me.ViewMenu.Text = "&View"
        '
        'TestModeToolStripMenuItem
        '
        Me.TestModeToolStripMenuItem.Name = "TestModeToolStripMenuItem"
        Me.TestModeToolStripMenuItem.Size = New System.Drawing.Size(127, 22)
        Me.TestModeToolStripMenuItem.Text = "TestMode"
        '
        'ShowToolStripMenuItem
        '
        Me.ShowToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DataToolStripMenuItem, Me.GraphToolStripMenuItem1, Me.PreviousTestToolStripMenuItem, Me.NTSAdjustableViewsToolStripMenuItem})
        Me.ShowToolStripMenuItem.Name = "ShowToolStripMenuItem"
        Me.ShowToolStripMenuItem.Size = New System.Drawing.Size(48, 20)
        Me.ShowToolStripMenuItem.Text = "Show"
        '
        'DataToolStripMenuItem
        '
        Me.DataToolStripMenuItem.Image = CType(resources.GetObject("DataToolStripMenuItem.Image"), System.Drawing.Image)
        Me.DataToolStripMenuItem.Name = "DataToolStripMenuItem"
        Me.DataToolStripMenuItem.Size = New System.Drawing.Size(188, 22)
        Me.DataToolStripMenuItem.Text = "Data"
        '
        'GraphToolStripMenuItem1
        '
        Me.GraphToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.InputSpecificTestcodeToolStripMenuItem})
        Me.GraphToolStripMenuItem1.Image = CType(resources.GetObject("GraphToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.GraphToolStripMenuItem1.Name = "GraphToolStripMenuItem1"
        Me.GraphToolStripMenuItem1.Size = New System.Drawing.Size(188, 22)
        Me.GraphToolStripMenuItem1.Text = "Graph"
        '
        'InputSpecificTestcodeToolStripMenuItem
        '
        Me.InputSpecificTestcodeToolStripMenuItem.Name = "InputSpecificTestcodeToolStripMenuItem"
        Me.InputSpecificTestcodeToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.InputSpecificTestcodeToolStripMenuItem.Text = "Input Specific Testcode"
        '
        'PreviousTestToolStripMenuItem
        '
        Me.PreviousTestToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1})
        Me.PreviousTestToolStripMenuItem.Image = CType(resources.GetObject("PreviousTestToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PreviousTestToolStripMenuItem.Name = "PreviousTestToolStripMenuItem"
        Me.PreviousTestToolStripMenuItem.Size = New System.Drawing.Size(188, 22)
        Me.PreviousTestToolStripMenuItem.Text = "Data and Graph"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(67, 22)
        '
        'NTSAdjustableViewsToolStripMenuItem
        '
        Me.NTSAdjustableViewsToolStripMenuItem.Image = CType(resources.GetObject("NTSAdjustableViewsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.NTSAdjustableViewsToolStripMenuItem.Name = "NTSAdjustableViewsToolStripMenuItem"
        Me.NTSAdjustableViewsToolStripMenuItem.Size = New System.Drawing.Size(188, 22)
        Me.NTSAdjustableViewsToolStripMenuItem.Text = "NTS Adjustable Views"
        '
        'ToolsMenu
        '
        Me.ToolsMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.comtoolportmenuitem, Me.TestDelaySettingsToolStripMenuItem, Me.DataBaseSettingsToolStripMenuItem})
        Me.ToolsMenu.Name = "ToolsMenu"
        Me.ToolsMenu.Size = New System.Drawing.Size(48, 20)
        Me.ToolsMenu.Text = "&Tools"
        '
        'comtoolportmenuitem
        '
        Me.comtoolportmenuitem.Image = CType(resources.GetObject("comtoolportmenuitem.Image"), System.Drawing.Image)
        Me.comtoolportmenuitem.Name = "comtoolportmenuitem"
        Me.comtoolportmenuitem.Size = New System.Drawing.Size(173, 22)
        Me.comtoolportmenuitem.Text = "Port:"
        '
        'TestDelaySettingsToolStripMenuItem
        '
        Me.TestDelaySettingsToolStripMenuItem.Name = "TestDelaySettingsToolStripMenuItem"
        Me.TestDelaySettingsToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.TestDelaySettingsToolStripMenuItem.Text = "Test Delay Settings"
        '
        'DataBaseSettingsToolStripMenuItem
        '
        Me.DataBaseSettingsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CreateStandAloneDatabaseToolStripMenuItem, Me.NTSDataDumpToolStripMenuItem})
        Me.DataBaseSettingsToolStripMenuItem.Name = "DataBaseSettingsToolStripMenuItem"
        Me.DataBaseSettingsToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.DataBaseSettingsToolStripMenuItem.Text = "DataBase Settings"
        '
        'CreateStandAloneDatabaseToolStripMenuItem
        '
        Me.CreateStandAloneDatabaseToolStripMenuItem.Name = "CreateStandAloneDatabaseToolStripMenuItem"
        Me.CreateStandAloneDatabaseToolStripMenuItem.Size = New System.Drawing.Size(223, 22)
        Me.CreateStandAloneDatabaseToolStripMenuItem.Text = "Create StandAlone Database"
        '
        'NTSDataDumpToolStripMenuItem
        '
        Me.NTSDataDumpToolStripMenuItem.Name = "NTSDataDumpToolStripMenuItem"
        Me.NTSDataDumpToolStripMenuItem.Size = New System.Drawing.Size(223, 22)
        Me.NTSDataDumpToolStripMenuItem.Text = "NTS DataDump"
        '
        'WindowsMenu
        '
        Me.WindowsMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CascadeToolStripMenuItem, Me.TileVerticalToolStripMenuItem, Me.TileHorizontalToolStripMenuItem, Me.CloseAllToolStripMenuItem})
        Me.WindowsMenu.Name = "WindowsMenu"
        Me.WindowsMenu.Size = New System.Drawing.Size(68, 20)
        Me.WindowsMenu.Text = "&Windows"
        '
        'CascadeToolStripMenuItem
        '
        Me.CascadeToolStripMenuItem.Name = "CascadeToolStripMenuItem"
        Me.CascadeToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.CascadeToolStripMenuItem.Text = "&Cascade"
        '
        'TileVerticalToolStripMenuItem
        '
        Me.TileVerticalToolStripMenuItem.Name = "TileVerticalToolStripMenuItem"
        Me.TileVerticalToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.TileVerticalToolStripMenuItem.Text = "Tile &Vertical"
        '
        'TileHorizontalToolStripMenuItem
        '
        Me.TileHorizontalToolStripMenuItem.Name = "TileHorizontalToolStripMenuItem"
        Me.TileHorizontalToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.TileHorizontalToolStripMenuItem.Text = "Tile &Horizontal"
        '
        'CloseAllToolStripMenuItem
        '
        Me.CloseAllToolStripMenuItem.Name = "CloseAllToolStripMenuItem"
        Me.CloseAllToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.CloseAllToolStripMenuItem.Text = "C&lose All"
        '
        'HelpMenu
        '
        Me.HelpMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem, Me.ArduinoDataStringsToolStripMenuItem})
        Me.HelpMenu.Name = "HelpMenu"
        Me.HelpMenu.Size = New System.Drawing.Size(44, 20)
        Me.HelpMenu.Text = "&Help"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(183, 22)
        Me.AboutToolStripMenuItem.Text = "&About ..."
        '
        'ArduinoDataStringsToolStripMenuItem
        '
        Me.ArduinoDataStringsToolStripMenuItem.Name = "ArduinoDataStringsToolStripMenuItem"
        Me.ArduinoDataStringsToolStripMenuItem.Size = New System.Drawing.Size(183, 22)
        Me.ArduinoDataStringsToolStripMenuItem.Text = "Arduino Data Strings"
        '
        'DateandtimeToolStripMenuItem
        '
        Me.DateandtimeToolStripMenuItem.Name = "DateandtimeToolStripMenuItem"
        Me.DateandtimeToolStripMenuItem.Size = New System.Drawing.Size(86, 20)
        Me.DateandtimeToolStripMenuItem.Text = "dateandtime"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'Mparent
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(632, 453)
        Me.Controls.Add(Me.MenuStrip)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip
        Me.Name = "Mparent"
        Me.Text = "NTS Transistor Tester System v1.0"
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents HelpMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CloseAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WindowsMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CascadeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TileVerticalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TileHorizontalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents ViewMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolsMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents comtoolportmenuitem As ToolStripMenuItem
    Friend WithEvents TestModeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AddKGUsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RemoveCustomKGUsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ShowToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GraphToolStripMenuItem1 As ToolStripMenuItem
    Public WithEvents parentPortcheck As IO.Ports.SerialPort
    Friend WithEvents DataToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrintToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TestDelaySettingsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DataBaseSettingsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CreateStandAloneDatabaseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NTSDataDumpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PreviousTestToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents InputSpecificTestcodeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RemoveTestsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RemoveSavedKGUToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Timer1 As Timer
    Friend WithEvents DateandtimeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NTSAdjustableViewsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ArduinoDataStringsToolStripMenuItem As ToolStripMenuItem
End Class
