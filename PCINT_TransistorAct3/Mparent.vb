﻿Imports System.Windows.Forms
Imports System.Data.SQLite

Public Class Mparent
    Public ActiveComport As String = ""
    Public Activetestcode As String = ""
    Public Activekgu As String = ""
    Public MDIForm1 As New Form1
    Dim includedatagrid As Boolean = False
    Dim datagridonly As Boolean = False
    Public delayvalue = 0


    Private Sub ExitToolsStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.Close()
    End Sub
    Private Sub CascadeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CascadeToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub TileVerticalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TileVerticalToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub TileHorizontalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TileHorizontalToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.TileHorizontal)
    End Sub
    Private Sub CloseAllToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CloseAllToolStripMenuItem.Click
        ' Close all child forms of the parent.
        For Each ChildForm As Form In Me.MdiChildren
            ChildForm.Close()
        Next
    End Sub

    Private m_ChildFormNumber As Integer
    Private Sub Mparent_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        refreshport()
        Initprevioustestcodeupdate(PreviousTestToolStripMenuItem)
        parentPortcheck.Close()
        runREMOVEKGU()
    End Sub
    Public Sub generategraphmenu(menu As ToolStripMenuItem, Optional ByVal itemtag As String = Nothing)
        menu.DropDownItems.Add("Show ALL", Nothing, AddressOf currenttestcodehandler)
        menu.DropDownItems.Add("Show Voltages Only", Nothing, AddressOf currenttestcodehandler)
        menu.DropDownItems.Add("Show Current Only", Nothing, AddressOf currenttestcodehandler)
        menu.DropDownItems.Add("Beta", Nothing, AddressOf currenttestcodehandler)
        menu.DropDownItems.Add("V|C", Nothing, AddressOf currenttestcodehandler)
        menu.DropDownItems.Add("V|RE", Nothing, AddressOf currenttestcodehandler)
        menu.DropDownItems.Add("V|CE", Nothing, AddressOf currenttestcodehandler)
        menu.DropDownItems.Add("V|RC", Nothing, AddressOf currenttestcodehandler)
        menu.DropDownItems.Add("V|Base", Nothing, AddressOf currenttestcodehandler)
        menu.DropDownItems.Add("I|Base", Nothing, AddressOf currenttestcodehandler)
        menu.DropDownItems.Add("I|Collector", Nothing, AddressOf currenttestcodehandler)
        menu.DropDownItems.Add("I|Emitter", Nothing, AddressOf currenttestcodehandler)

    End Sub

    Public Sub runallformsgbox(Optional show As String = "")
        Dim mdiViewdata1 As New viewData("V|C")
        mdiViewdata1.MdiParent = Me
        mdiViewdata1.Show()
        Dim mdiViewdata2 As New viewData("V|RE")
        mdiViewdata2.MdiParent = Me
        mdiViewdata2.Show()
        Dim mdiViewdata3 As New viewData("V|CE")
        mdiViewdata3.MdiParent = Me
        mdiViewdata3.Show()
        Dim mdiViewdata4 As New viewData("V|RC")
        mdiViewdata4.MdiParent = Me
        mdiViewdata4.Show()
        Dim mdiViewdata5 As New viewData("V|Base")
        mdiViewdata5.MdiParent = Me
        mdiViewdata5.Show()
        Dim mdiViewdata6 As New viewData("I|Base")
        mdiViewdata6.MdiParent = Me
        mdiViewdata6.Show()
        Dim mdiViewdata7 As New viewData("I|Collector")
        mdiViewdata7.MdiParent = Me
        mdiViewdata7.Show()
        Dim mdiViewdata8 As New viewData("I|Emitter")
        mdiViewdata8.MdiParent = Me
        mdiViewdata8.Show()
        Dim mdiViewdata9 As New viewData("Beta")
        mdiViewdata9.MdiParent = Me
        mdiViewdata9.Show()

        If (show = "showall") Then
            Dim mdiDatadump As New DataGrid("showall", Activetestcode)
            mdiDatadump.MdiParent = Me
            mdiDatadump.Show()
        End If

    End Sub
    Public Sub currenttestcodehandler(sender As Object, e As EventArgs, Optional input As Boolean = False)
        Dim graphtest = DirectCast(sender, ToolStripMenuItem)
        If (Me.Activetestcode = "") Then

            MsgBox("No Current test", MsgBoxStyle.Critical, "Error")
            Return
        End If
        If (datagridonly) Then
            If (input) Then
                Dim mdiDatadump As New DataGrid("showall", Activetestcode)
                mdiDatadump.MdiParent = Me
                mdiDatadump.Show()
                Return
            Else
                Dim gridtest = DirectCast(sender, ToolStripMenuItem)
                If (gridtest.Text = "Show ALL") Then
                    Dim mdiDatadump As New DataGrid("showall", Activetestcode)
                    mdiDatadump.MdiParent = Me
                    mdiDatadump.Show()
                ElseIf (gridtest.Text = "Show Voltages Only") Then
                    Dim mdiDatadump As New DataGrid("svo", Activetestcode)
                    mdiDatadump.MdiParent = Me
                    mdiDatadump.Show()

                ElseIf (gridtest.Text = "Show Current Only") Then
                    Dim mdiDatadump As New DataGrid("sco", Activetestcode)
                    mdiDatadump.MdiParent = Me
                    mdiDatadump.Show()
                Else
                    Dim mdiDatadump As New DataGrid(gridtest.Text, Activetestcode)
                    mdiDatadump.MdiParent = Me
                    mdiDatadump.Show()
                End If
                Return
            End If
        End If
        If (includedatagrid = True) Then
            If (input) Then
                Dim mdiDatadump As New DataGrid("showall", Activetestcode)
                mdiDatadump.MdiParent = Me
                mdiDatadump.Show()
                runallformsgbox()
                Return
            Else
                If (graphtest.Text = "Show ALL") Then
                ElseIf (graphtest.Text = "Show Voltages Only") Then
                    Dim mdiDatadump As New DataGrid("svo", Activetestcode)
                    mdiDatadump.MdiParent = Me
                    mdiDatadump.Show()

                ElseIf (graphtest.Text = "Show Current Only") Then
                    Dim mdiDatadump As New DataGrid("sco", Activetestcode)
                    mdiDatadump.MdiParent = Me
                    mdiDatadump.Show()
                Else
                    Dim mdiDatadump As New DataGrid(graphtest.Text, Activetestcode)
                    mdiDatadump.MdiParent = Me
                    mdiDatadump.Show()
                End If
            End If
        End If

        If (input) Then
            runallformsgbox()
            Return
        End If

        If (graphtest.Text = "Show ALL") Then
            runallformsgbox()
        ElseIf (graphtest.Text = "Show Voltages Only") Then
            Dim mdiViewdata1 As New viewData("V|C")
            mdiViewdata1.MdiParent = Me
            mdiViewdata1.Show()
            Dim mdiViewdata2 As New viewData("V|RE")
            mdiViewdata2.MdiParent = Me
            mdiViewdata2.Show()
            Dim mdiViewdata3 As New viewData("V|CE")
            mdiViewdata3.MdiParent = Me
            mdiViewdata3.Show()
            Dim mdiViewdata4 As New viewData("V|RC")
            mdiViewdata4.MdiParent = Me
            mdiViewdata4.Show()
            Dim mdiViewdata5 As New viewData("V|Base")
            mdiViewdata5.MdiParent = Me
            mdiViewdata5.Show()
        ElseIf (graphtest.Text = "Show Current Only") Then
            Dim mdiViewdata1 As New viewData("I|Base")
            mdiViewdata1.MdiParent = Me
            mdiViewdata1.Show()
            Dim mdiViewdata2 As New viewData("I|Collector")
            mdiViewdata2.MdiParent = Me
            mdiViewdata2.Show()
            Dim mdiViewdata3 As New viewData("I|Emitter")
            mdiViewdata3.MdiParent = Me
            mdiViewdata3.Show()
        Else
            Dim mdiViewdata As New viewData(graphtest.Text)
            mdiViewdata.MdiParent = Me
            mdiViewdata.Show()
        End If
    End Sub
    Private Sub refreshport()
        Me.comtoolportmenuitem.DropDownItems.Clear()
        Me.WindowState = FormWindowState.Maximized
        Dim ports As String() = parentPortcheck.GetPortNames()
        Dim port As String
        For Each port In ports
            comtoolportmenuitem.DropDownItems.Add(port, Nothing, AddressOf comport_Select)
        Next port
    End Sub
    Private Sub comport_Select(sender As Object, e As EventArgs)
        Dim MDIpcomPortActive = DirectCast(sender, ToolStripMenuItem)
        ActiveComport = MDIpcomPortActive.Text
        comtoolportmenuitem.Text = "Port: " & MDIpcomPortActive.Text
        MDIpcomPortActive.Checked = True
        ViewMenu.Enabled = True
    End Sub
    Private Sub TestModeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TestModeToolStripMenuItem.Click
        For Each f As Form In Application.OpenForms
            If TypeOf f Is Form1 Then
                f.Activate()
                Return
            End If
        Next
        TestModeToolStripMenuItem.Checked = True
        Dim MDIForm1 As New Form1
        MDIForm1.MdiParent = Me
        MDIForm1.Show()
    End Sub

    Private Sub AddKGUsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddKGUsToolStripMenuItem.Click
        Dim myvalue As String
        myvalue = InputBox("Enter a New KGU to Test", "New KGU", "", 100, 100)
        If myvalue IsNot "" Then
            Dim conmakedb As SQLiteConnection = New SQLiteConnection("NTSTransdata.sqlite")
            Dim conn = New SQLiteConnection("Data Source = NTSTransdata.sqlite;Version=3")
            conn.Open()


            Try
                Dim querygraph As String = "INSERT INTO KGUdataLIST (Transistorname) VALUES ('" & myvalue & "')"
                Console.Write(querygraph)
                Dim cmd1 As SQLiteCommand = New SQLiteCommand(querygraph, conn)
                cmd1.ExecuteReader()
                conn.Close()
                MsgBox("Succesfully Added the Transistor " + myvalue + " Into KGU")
            Catch ex As Exception
                Console.Write(ex.Message)
            End Try
            conn.Close()
        End If

        runREMOVEKGU()
    End Sub
    Private Sub runREMOVEKGU()
        Dim mysqlcongraph = New SQLiteConnection
        mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
        Try
            mysqlcongraph.Open()
            Dim querygraph As String = "Select DISTINCT Transistorname FROM KGUdataLIST "
            Dim Command = New SQLiteCommand(querygraph, mysqlcongraph)
            Dim reader = Command.ExecuteReader()

            Command = New SQLiteCommand(querygraph, mysqlcongraph)
            RemoveCustomKGUsToolStripMenuItem.DropDownItems.Clear()
            While reader.Read
                Me.RemoveCustomKGUsToolStripMenuItem.DropDownItems.Add(reader("Transistorname"), Nothing, AddressOf REMOVETHISTRANS)
            End While
            mysqlcongraph.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        End Try
    End Sub
    Private Sub REMOVETHISTRANS(sender As Object, e As EventArgs)
        Dim kgu = DirectCast(sender, ToolStripMenuItem)
        Dim mysqlcongraph = New SQLiteConnection
        mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
        Try
            mysqlcongraph.Open()
            Dim querygraph As String = "DELETE FROM KGUdataLIST where Transistorname = '" + kgu.Text + "'"
            Dim Command = New SQLiteCommand(querygraph, mysqlcongraph)
            Dim reader = Command.ExecuteReader()
            Console.WriteLine(querygraph)
            Command = New SQLiteCommand(querygraph, mysqlcongraph)
            While reader.Read
                Me.RemoveCustomKGUsToolStripMenuItem.DropDownItems.Add(reader("Transistorname"), Nothing, AddressOf REMOVETHISTRANS)
            End While
            mysqlcongraph.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        End Try
    End Sub
    Private Sub kguset(sender As Object, e As EventArgs)
        Dim kgu = DirectCast(sender, ToolStripMenuItem)
        MDIForm1.setkgutodb(kgu.Text)
    End Sub
    Private Sub CreateStandAloneDatabaseToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CreateStandAloneDatabaseToolStripMenuItem.Click
        Dim conmakedb As SQLiteConnection = New SQLiteConnection("NTSTransdata.sqlite")
        Dim conn = New SQLiteConnection("Data Source = NTSTransdata.sqlite;Version=3")
        conn.Open()

        Dim sql As String = "CREATE TABLE IF NOT EXISTS transdata (
                                id INTEGER PRIMARY KEY AUTOINCREMENT,
                                rb varchar(45) NOT NULL,rc varchar(45) NOT NULL,
                                re varchar(45) NOT NULL,
                                Transistorname varchar(45) NOT NULL,
                                vc float DEFAULT NULL,
                                vre float DEFAULT NULL,
                                vce float DEFAULT NULL,
                                vrc float DEFAULT NULL,
                                vb float DEFAULT NULL,
                                ib float DEFAULT NULL,
                                ic float DEFAULT NULL,
                                ie float DEFAULT NULL,
                                B float DEFAULT NULL,
                                iskgu varchar(45) DEFAULT 'NO',
                                Dtime datetime DEFAULT NULL,
                                Trial int(11) DEFAULT '0',
                                Testcode Int(11) DEFAULT NULL)"
        Dim cmd1 As SQLiteCommand = New SQLiteCommand(sql, conn)

        Try
            cmd1.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        conn.Close()

        Dim conmakedb1 As SQLiteConnection = New SQLiteConnection("NTSTransdata.sqlite")
        Dim conn1 = New SQLiteConnection("Data Source = NTSTransdata.sqlite;Version=3")
        conn1.Open()

        Dim sql1 As String = "CREATE TABLE IF NOT EXISTS KGUdataLIST (
                                id INTEGER PRIMARY KEY AUTOINCREMENT,
                                Transistorname varchar(45) NOT NULL)"
        Console.WriteLine(sql1)
        Dim cmd11 As SQLiteCommand = New SQLiteCommand(sql1, conn1)

        Try
            cmd11.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        conn1.Close()

        Dim conmakedb2 As SQLiteConnection = New SQLiteConnection("NTSTransdata.sqlite")
        Dim conn2 = New SQLiteConnection("Data Source = NTSTransdata.sqlite;Version=3")
        conn2.Open()

        Dim sql2 As String = "INSERT INTO KGUdataLIST (Transistorname) Values('2n2222'),('C828'),('TIP31c')"
        Console.WriteLine(sql1)
        Dim cmd2 As SQLiteCommand = New SQLiteCommand(sql2, conn2)

        Try
            cmd2.ExecuteReader()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        conn2.Close()


        MsgBox("Database Creation was Done", MsgBoxStyle.OkOnly, "Done")
    End Sub
    Private Sub NTSDataDumpToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NTSDataDumpToolStripMenuItem.Click
        Dim mdiDatadump As New DataGrid("datadump")
        mdiDatadump.MdiParent = Me
        mdiDatadump.Show()
    End Sub

    Private Sub NTSDataDumpToolStripMenuItem_MouseDown(sender As Object, e As MouseEventArgs) Handles NTSDataDumpToolStripMenuItem.MouseDown
        If (e.Button = MouseButtons.Right) Then
            Dim mdiDatadump As New DataGrid("datadump")
            mdiDatadump.Show()
        End If

    End Sub

    Private Sub Initprevioustestcodeupdate(tool As ToolStripMenuItem)
        Dim mysqlcongraph = New SQLiteConnection
        mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
        Try
            mysqlcongraph.Open()
            Dim querygraph As String = "Select DISTINCT Testcode, Transistorname,Dtime FROM transdata where iskgu='NO' "
            Dim adapter As New SQLiteDataAdapter
            Dim Command = New SQLiteCommand(querygraph, mysqlcongraph)
            Dim reader = Command.ExecuteReader()
            tool.DropDownItems.Clear()
            Command = New SQLiteCommand(querygraph, mysqlcongraph)
            adapter.SelectCommand = Command
            Dim index As Integer = 1
            DataToolStripMenuItem.DropDownItems.Clear()
            DataToolStripMenuItem.DropDownItems.Add("Input Specific BatchCode", Nothing, AddressOf InputSpecificTestcodeToolStripMenu)
            GraphToolStripMenuItem1.DropDownItems.Add("Input Specific BatchCode", Nothing, AddressOf InputSpecificTestcodeToolStripMenu)
            PreviousTestToolStripMenuItem.DropDownItems.Add("Input Specific BatchCode", Nothing, AddressOf InputSpecificTestcodeToolStripMenu)
            While reader.Read
                tool.DropDownItems.Add(reader("Testcode") & " | " & reader("Transistorname") & " | " & reader("Dtime"), Nothing, Nothing)
                generategraphmenu(tool.DropDownItems.Item(index))
                AddHandler tool.DropDownItems.Item(index).MouseHover, AddressOf hovertest
                index += 1
            End While
            mysqlcongraph.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        End Try
    End Sub
    Public Sub hovertest(sender As Object, e As EventArgs)
        Dim toolmenucast = DirectCast(sender, ToolStripMenuItem)
        Try
            Dim seted() As String = Split(toolmenucast.Text, " | ", 3)
            Me.Activetestcode = seted(0)
            Me.Activekgu = seted(1)
        Catch
            Return
        End Try
    End Sub
    Private Sub PreviousTestcodeToolStripMenuItem_MouseHover(sender As Object, e As EventArgs)
        Initprevioustestcodeupdate(sender)
    End Sub
    Private Sub InputSpecificTestcodeToolStripMenu(sender As Object, e As EventArgs)
        Dim myvalue As String
        myvalue = InputBox("Please Enter The Specific TestCode", "Enter the Testcode You want to View", 100, 100)
        If myvalue IsNot "" Then
            Dim mysqlcongraph = New SQLiteConnection
            mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
            Try
                mysqlcongraph.Open()
                Dim querygraph As String = "Select COUNT(Testcode), Transistorname FROM transdata where Testcode=" & myvalue
                Dim adapter As New SQLiteDataAdapter
                Dim Command = New SQLiteCommand(querygraph, mysqlcongraph)
                Dim reader = Command.ExecuteReader()
                Command = New SQLiteCommand(querygraph, mysqlcongraph)
                adapter.SelectCommand = Command
                While reader.Read
                    If (reader("COUNT(Testcode)") >= 1) Then
                        Me.Activetestcode = myvalue
                        currenttestcodehandler(sender, Nothing, True)
                    Else
                        MsgBox("TestCode doesnt exist", MsgBoxStyle.Critical, "ERROR")
                    End If
                End While
                mysqlcongraph.Close()
            Catch ex As Exception
                Console.Write(ex.Message)
            End Try

        Else
            MsgBox("Please Input A valid Testcode", MsgBoxStyle.Critical, "ERROR")
        End If
    End Sub

    Private Sub RemoveTestsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RemoveTestsToolStripMenuItem.Click
        Dim myvalue As String
        myvalue = InputBox("Please Enter The Specific TestCode", "Enter the Testcode You want to DELETE", 100, 100)
        If myvalue IsNot "" Then
            Dim answer As String = MsgBox("Are you sure you want to delete testcode" & myvalue & "All Data Will be deleted", MsgBoxStyle.YesNo, "Are You Sure!!")
            If (answer = vbYes) Then

                Dim mysqlcongraph = New SQLiteConnection
                mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
                Try
                    mysqlcongraph.Open()
                    Dim querygraph As String = "DELETE FROM transdata where Testcode='" & myvalue & "'"
                    Dim adapter As New SQLiteDataAdapter
                    Dim Command = New SQLiteCommand(querygraph, mysqlcongraph)
                    Dim reader = Command.ExecuteReader()
                    Command = New SQLiteCommand(querygraph, mysqlcongraph)
                    adapter.SelectCommand = Command

                    mysqlcongraph.Close()
                Catch ex As Exception
                    Console.Write(ex.Message)
                End Try
            End If
            MsgBox("Testcode Successfully deleted", MsgBoxStyle.OkOnly, "DELETED")
        End If
    End Sub
    Private Sub PreviousTestToolStripMenuItem_MouseHover(sender As Object, e As EventArgs) Handles PreviousTestToolStripMenuItem.MouseHover
        includedatagrid = True
        datagridonly = False
        Initprevioustestcodeupdate(sender)
    End Sub
    Private Sub GraphToolStripMenuItem1_MouseHover(sender As Object, e As EventArgs) Handles GraphToolStripMenuItem1.MouseHover
        includedatagrid = False
        datagridonly = False
        Initprevioustestcodeupdate(sender)
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        DateandtimeToolStripMenuItem.Text = DateTime.Now.ToString(" | dddd, dd MMMM yyyy | hh:mm:ss")
    End Sub

    Private Sub TestDelaySettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TestDelaySettingsToolStripMenuItem.Click
        Dim myvalue As String
        myvalue = InputBox("Please Enter The Delay Interval in ms | Setting test delay function will halt/freeze the entire application during delays use cautiously", "Delay", 100, 100)
        If myvalue IsNot "" Then
            Try
                delayvalue = Integer.Parse(myvalue)
                MsgBox("Delay Succesfuly Configured", MsgBoxStyle.OkOnly, "")

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub RemoveCustomKGUsToolStripMenuItemhover(sender As Object, e As EventArgs) Handles RemoveCustomKGUsToolStripMenuItem.MouseHover
        runREMOVEKGU()
    End Sub
    Private Sub NTSAdjustableViewsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NTSAdjustableViewsToolStripMenuItem.Click
        Dim ntsdata As New ntsadjust
        ntsdata.MdiParent = Me
        ntsdata.Show()
    End Sub

    Private Sub PrintToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PrintToolStripMenuItem.Click
        Dim print As New printdialog
        print.MdiParent = Me
        print.Show()
    End Sub
    Private Sub DataToolStripMenuItem_MouseHover(sender As Object, e As EventArgs) Handles DataToolStripMenuItem.MouseHover
        Initprevioustestcodeupdate(sender)
        datagridonly=true
    End Sub

    Private Sub ToolsMenu_Click(sender As Object, e As EventArgs) Handles ToolsMenu.Click
        refreshport()
    End Sub

    Private Sub RemoveSavedKGUToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RemoveSavedKGUToolStripMenuItem.Click
        Dim mysqlcongraph = New SQLiteConnection
        mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
        Try
            mysqlcongraph.Open()
            Dim querygraph As String = "DELETE FROM transdata where iskgu='YES' "
            Dim adapter As New SQLiteDataAdapter
            Dim Command = New SQLiteCommand(querygraph, mysqlcongraph)
            Dim reader = Command.ExecuteReader()
            Command = New SQLiteCommand(querygraph, mysqlcongraph)
            adapter.SelectCommand = Command
            mysqlcongraph.Close()
            MsgBox("Done")
        Catch ex As Exception
            Console.Write(ex.Message)
        End Try
    End Sub

    Private Sub DateandtimeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DateandtimeToolStripMenuItem.Click

    End Sub

    Private Sub DataToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DataToolStripMenuItem.Click

    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        AboutBox1.Show()
    End Sub

    Private Sub ArduinoDataStringsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ArduinoDataStringsToolStripMenuItem.Click
        Form2.Show()
    End Sub
End Class
