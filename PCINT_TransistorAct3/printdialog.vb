﻿Imports System.Drawing.Printing

Public Class printdialog
    Private WithEvents docToPrint As New Printing.PrintDocument
    Dim firstrun = True
    Dim populategraph = False
    Dim graphcount As Integer
    Dim kgucount = 0
    Dim multiplied
    Dim multipler As Integer
    Dim datatorun
    Dim units
    Dim controsl = False
    Dim controsls = False
    Dim dtime
    Dim testcode
    Dim querygraph2 As String
    Dim mRow As Integer = 0
    Dim adapter As New SQLiteDataAdapter
    Dim dataset As New DataTable
    Dim source As New BindingSource
    Dim dgfirstrun = True
    Dim transtest
    Dim paramrb
    Dim paramrc
    Dim paramre





    Private Sub printdialog_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim mysqlcongraph = New SQLiteConnection
        mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
        Try
            mysqlcongraph.Open()
            Dim querygraph As String = "Select DISTINCT Testcode, Transistorname,Dtime FROM transdata where iskgu='NO' "
            Dim adapter As New SQLiteDataAdapter
            Dim Command = New SQLiteCommand(querygraph, mysqlcongraph)
            Dim reader = Command.ExecuteReader()
            Command = New SQLiteCommand(querygraph, mysqlcongraph)
            adapter.SelectCommand = Command
            Dim index As Integer = 0
            While reader.Read
                ComboBox1.Items.Add(reader("Testcode") & " | " & reader("Transistorname") & " | " & reader("Dtime"))
            End While
            mysqlcongraph.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        End Try
        Button1.Enabled = False
        Button2.Enabled = False
        GroupBox1.Enabled = False
        GroupBox2.Enabled = False
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        If (CheckBox16.Checked = True) Then
            controsl = True
            CheckBox16.Checked = False
        End If
        If (CheckBox14.Checked = True) Then
            controsls = True
            CheckBox14.Checked = False
        End If
        ' Allow the user to choose the page range he or she would
        ' like to print.
        PrintDialog1.AllowSomePages = True

        ' Show the help button.
        PrintDialog1.ShowHelp = True

        ' Set the Document property to the PrintDocument for 
        ' which the PrintPage Event has been handled. To display the
        ' dialog, either this property or the PrinterSettings property 
        ' must be set 
        Dim firstrun = True

        PrintDialog1.Document = docToPrint




        Dim result As DialogResult = PrintDialog1.ShowDialog()

        If (result = DialogResult.OK) Then
            docToPrint.Print()
        End If

        ' Create and initialize print font 
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub document_PrintPage(ByVal sender As Object,
   ByVal e As System.Drawing.Printing.PrintPageEventArgs) _
       Handles docToPrint.PrintPage

        Dim printFont As New System.Drawing.Font("Arial", 20, System.Drawing.FontStyle.Regular)
        Dim myRec As New System.Drawing.Rectangle(0, -600, 920, 600)

        If (firstrun) Then
            e.Graphics.DrawString("NTS Transistor Tester V1", printFont, Brushes.Black, 250, 100)
            e.Graphics.DrawString(ComboBox1.Text, printFont, Brushes.Black, 200, 150)
            e.Graphics.DrawString(DateTime.Now, printFont, Brushes.Black, 250, 250)
            e.HasMorePages = True
            populategraph = True
            firstrun = False
            Return
        ElseIf (populategraph) Then
            Dim seted() As String = Split(ComboBox1.Text, " | ", 3)
            testcode = seted(0)
            transtest = seted(1)

            For Each ctrl As Control In Me.GroupBox1.Controls
                If TypeOf ctrl Is CheckBox Then
                    Console.WriteLine(ctrl.Text)
                    If (DirectCast(ctrl, CheckBox).Checked = True) Then
                        Chart1.Series(0).Points.Clear()
                        Chart1.Series(1).Points.Clear()
                        Select Case ctrl.Text
                            Case "B"
                                multipler = 1
                                Name = "Beta"
                                datatorun = "B"
                                units = ""
                            Case "VC"
                                multipler = 1
                                Name = "Voltage In Collector"
                                datatorun = "vc"
                                units = "in Volts"
                            Case "VRE"
                                multipler = 1000
                                Name = "Voltage In Emitter"
                                datatorun = "vre"
                                units = "in milliVolts"
                            Case "VCE"
                                multipler = 1000
                                Name = "Voltage In Collector Emitter"
                                datatorun = "vce"
                                units = "in milliVolts"
                            Case "VRC"
                                multipler = 1
                                Name = "Voltage In R Collector"
                                datatorun = "vrc"
                                units = "in Volts"
                            Case "VB"
                                multipler = 1
                                Name = "Voltage In Base"
                                datatorun = "vb"
                                units = "in Volts"
                            Case "IB"
                                multipler = 1000000
                                Name = "Current In Base"
                                datatorun = "ib"
                                units = "in microAmps"
                            Case "IC"
                                multipler = 1000
                                Name = "Current In Collector"
                                datatorun = "ic"
                                units = "in milliAmps"
                            Case "IE"
                                multipler = 1000
                                Name = "Current In Emitter"
                                datatorun = "ie"
                                units = "in milliAmps"
                        End Select
                        Chart1.Titles("TopName").Text = Name
                        Chart1.ChartAreas(0).AxisY.Title = units

                        Dim quequery As String = datatorun
                        Dim chartkgu = 0
                        Dim count = 0

                        Try
                            Dim mysqlcongraph2 = New SQLiteConnection
                            mysqlcongraph2.ConnectionString = My.Resources.sqllogindetails
                            mysqlcongraph2.Open()
                            Dim querygraph As String = "Select Trial,Dtime,rb,rc,re, " & quequery & " FROM transdata Where iskgu = 'NO' AND Testcode='" & testcode & "'" ' Where Transistorname=" + Form1.kguintest
                            Dim Command = New SQLiteCommand(querygraph, mysqlcongraph2)
                            Dim reader = Command.ExecuteReader()


                            While reader.Read
                                paramrb = reader("rb")
                                paramrc = reader("rc")
                                paramre = reader("re")
                                dtime = reader("Dtime")
                                multiplied = reader(quequery)
                                multiplied = multiplied * multipler
                                Chart1.Series(1).Points.AddXY(reader("Trial").ToString, multiplied)
                                kgucount = kgucount + 1
                            End While
                            mysqlcongraph2.Close()

                            Dim mysqlcongraph = New SQLiteConnection
                            mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
                            mysqlcongraph.Open()
                            Dim querygraph1 As String = "SELECT min(" & quequery & ") , max(" & quequery & ") FROM transdata Where iskgu = 'NO' AND Testcode='" & testcode & "'"
                            Command = New SQLiteCommand(querygraph1, mysqlcongraph)
                            reader = Command.ExecuteReader

                            While reader.Read
                                multiplied = reader("min(" & quequery & ")")
                                multiplied = multiplied * multipler
                                multiplied = Math.Round(multiplied, 0)
                                Chart1.ChartAreas("data1").AxisY.Minimum = multiplied - 20
                                Chart1.ChartAreas("data1").AxisY2.Minimum = multiplied - 20

                                multiplied = reader("max(" & quequery & ")")
                                multiplied = multiplied * multipler
                                multiplied = Math.Round(multiplied, 0)
                                Chart1.ChartAreas("data1").AxisY.Maximum = multiplied + 20
                                Chart1.ChartAreas("data1").AxisY2.Maximum = multiplied + 20

                            End While


                            mysqlcongraph.Close()
                        Catch ex As Exception
                        End Try

                        Dim mysqlcongraph1 = New SQLiteConnection
                        mysqlcongraph1.ConnectionString = My.Resources.sqllogindetails
                        Try
                            mysqlcongraph1.Open()
                            Dim querygraph As String = "Select " & quequery & " FROM transdata Where iskgu = 'YES' AND Transistorname='" & transtest & "'"
                            Dim Command = New SQLiteCommand(querygraph, mysqlcongraph1)
                            Dim reader = Command.ExecuteReader
                            multiplied = 0
                            While reader.Read
                                multiplied = reader(quequery)
                                multiplied = multiplied * multipler
                                chartkgu = multiplied
                            End While
                            While (kgucount >= count And multiplied <> 0)
                                Chart1.Series(0).Points.AddY(chartkgu.ToString)
                                count = count + 1
                            End While
                            mysqlcongraph1.Close()
                            count = 0
                            kgucount = 0
                        Catch ex As Exception
                        End Try
                        e.Graphics.TranslateTransform(100.0F, 100.0F)
                        e.Graphics.RotateTransform(90.0F)
                        e.Graphics.DrawString("NTS Equipment Corporation Systems Report", printFont, Brushes.Black, 0, -700)
                        e.Graphics.DrawString("Batch: " & testcode & " Transistorname: " & transtest & " TestDate: " & dtime, printFont, Brushes.Black, 0, -650)
                        Chart1.Printing.PrintPaint(e.Graphics, myRec)
                        e.Graphics.DrawString("Systems Report Generated at " + DateTime.Now.ToString(), printFont, Brushes.Black, 0, 0)
                        e.HasMorePages = True
                        DirectCast(ctrl, CheckBox).Checked = False
                        Return
                    End If

                End If
            Next
            populategraph = False
            e.HasMorePages = True
            Return
        Else
            querygraph2 = "Select Trial,Transistorname as 'Transistor'"
            runquery()

        End If

        querygraph2 = querygraph2 + " FROM transdata where testcode ='" & testcode & "'"
        Console.WriteLine(querygraph2)
        Dim mysqlcongraph5 = New SQLiteConnection
        mysqlcongraph5.ConnectionString = My.Resources.sqllogindetails
        Try
            mysqlcongraph5.Open()
            'querygraph As String = "Select * FROM transdata "
            Dim Command = New SQLiteCommand(querygraph2, mysqlcongraph5)
            Dim reader = Command.ExecuteReader()

            Command = New SQLiteCommand(querygraph2, mysqlcongraph5)
            adapter.SelectCommand = Command
            adapter.Fill(dataset)
            source.DataSource = dataset
            DataGridView1.DataSource = source
            DataGridView1.AutoResizeColumns()
            While reader.Read
                Dim inc As Integer = inc + 1
                DataGridView1.Columns(reader.GetName(inc)).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End While
            adapter.Update(dataset)
            DataGridView1.FirstDisplayedScrollingRowIndex += 1
            mysqlcongraph5.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        End Try

        e.Graphics.DrawString("Transistor Data Report", printFont, Brushes.Black, 300, 50)
        e.Graphics.DrawString("Batch: " & testcode & " Transistor: " & transtest, printFont, Brushes.Black, 250, 75)
        e.Graphics.DrawString(" TestDate: " & dtime, printFont, Brushes.Black, 230, 100)
        e.Graphics.DrawString("Parameters : RB= " & paramrb & " RC= " & paramrc & " RE= " & paramre, printFont, Brushes.Black, 50, 130)

        With DataGridView1
            Dim fmt As StringFormat = New StringFormat(StringFormatFlags.LineLimit)
            fmt.LineAlignment = StringAlignment.Center
            fmt.Trimming = StringTrimming.EllipsisCharacter
            Dim y As Single = e.MarginBounds.Top + 100
            Do While mRow < .RowCount
                Dim row As DataGridViewRow = .Rows(mRow)
                Dim x As Single = 20
                Dim h As Single = 0
                If (dgfirstrun Or e.HasMorePages) Then
                    For Each cell As DataGridViewCell In row.Cells
                        Dim rc As RectangleF = New RectangleF(x, y, cell.Size.Width, cell.Size.Height)
                        Dim rc2 As RectangleF = New RectangleF(x, y - 22, cell.Size.Width, cell.Size.Height)
                        e.Graphics.FillRectangle(New SolidBrush(Color.LightGray), New RectangleF(x, y - 22, cell.Size.Width, cell.Size.Height))
                        e.Graphics.DrawRectangle(Pens.Black, rc2.Left, rc2.Top, rc2.Width, rc2.Height)
                        e.Graphics.DrawString(DataGridView1.Columns(cell.ColumnIndex).HeaderText, .Font, Brushes.Black, rc2, fmt)
                        x += rc.Width
                    Next
                End If
                dgfirstrun = False
                x = 20
                For Each cell As DataGridViewCell In row.Cells
                    Dim rc As RectangleF = New RectangleF(x, y, cell.Size.Width, cell.Size.Height)
                    Dim rc2 As RectangleF = New RectangleF(x, y - 22, cell.Size.Width, cell.Size.Height)
                    If (e.HasMorePages) Then
                        e.Graphics.FillRectangle(New SolidBrush(Color.LightGray), New RectangleF(x, y - 22, cell.Size.Width, cell.Size.Height))
                        e.Graphics.DrawRectangle(Pens.Black, rc2.Left, rc2.Top, rc2.Width, rc2.Height)
                        e.Graphics.DrawString(DataGridView1.Columns(cell.ColumnIndex).HeaderText, printFont, Brushes.Black, rc2, fmt)
                    End If
                    e.Graphics.DrawRectangle(Pens.Black, rc.Left, rc.Top, rc.Width, rc.Height)
                    e.Graphics.DrawString(DataGridView1.Rows(cell.RowIndex).Cells(cell.ColumnIndex).FormattedValue.ToString, .Font, Brushes.Black, rc, fmt)
                    x += rc.Width
                    h = Math.Max(h, rc.Height)
                Next
                e.HasMorePages = False
                y += h
                mRow += 1
                If y + h > e.MarginBounds.Bottom Then
                    e.HasMorePages = True
                    mRow -= 1
                    e.HasMorePages = True
                    Exit Sub
                End If
            Loop
            mRow = 0
        End With

    End Sub
    Private Sub runquery()
        For Each ctrl As Control In Me.GroupBox2.Controls
            If TypeOf ctrl Is CheckBox Then
                If (DirectCast(ctrl, CheckBox).Checked = True) Then
                    If ctrl.Text = "B" Then querygraph2 = querygraph2 + ",B as Beta"
                    If ctrl.Text = "VC" Then querygraph2 = querygraph2 + ",vc as VC"
                    If ctrl.Text = "VRE" Then querygraph2 = querygraph2 + ",vre as VRE"
                    If ctrl.Text = "VCE" Then querygraph2 = querygraph2 + ",vce as VCE"
                    If ctrl.Text = "VRC" Then querygraph2 = querygraph2 + ",vrc as VRC"
                    If ctrl.Text = "VB" Then querygraph2 = querygraph2 + ",vb as VB"
                    If ctrl.Text = "IB" Then querygraph2 = querygraph2 + ",ib as iB"
                    If ctrl.Text = "IC" Then querygraph2 = querygraph2 + ",ic as iC"
                    If ctrl.Text = "IE" Then querygraph2 = querygraph2 + ",round(ie,5) as IE"
                    If ctrl.Text = "RB" Then querygraph2 = querygraph2 + ",rb as RB"
                    If ctrl.Text = "RC" Then querygraph2 = querygraph2 + ",rc as RC"
                    If ctrl.Text = "RE" Then querygraph2 = querygraph2 + ",re as RE"
                    DirectCast(ctrl, CheckBox).Checked = False
                End If
            End If


        Next
    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        If (CheckBox16.Checked = True) Then
            controsl = True
            CheckBox16.Checked = False
        End If
        If (CheckBox14.Checked = True) Then
            controsls = True
            CheckBox14.Checked = False
        End If

        PrintPreviewDialog1.Document = docToPrint
        PrintPreviewDialog1.Width = FormWindowState.Maximized
        Dim result As DialogResult = PrintPreviewDialog1.ShowDialog
        firstrun = True

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        Button1.Enabled = True
        Button2.Enabled = True
        GroupBox1.Enabled = True
        GroupBox2.Enabled = True

    End Sub

    Private Sub CheckBox16_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox16.CheckedChanged
        If (CheckBox16.Checked = True) Then
            For Each ctrl As Control In Me.GroupBox1.Controls
                If TypeOf ctrl Is CheckBox Then
                    DirectCast(ctrl, CheckBox).Checked = True
                End If
            Next
        ElseIf (controsl = False) Then
            For Each ctrl As Control In Me.GroupBox1.Controls
                If TypeOf ctrl Is CheckBox Then
                    DirectCast(ctrl, CheckBox).Checked = False
                End If
            Next
        End If
    End Sub

    Private Sub CheckBox14_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox14.CheckedChanged
        If (CheckBox14.Checked = True) Then
            For Each ctrl As Control In Me.GroupBox2.Controls
                If TypeOf ctrl Is CheckBox Then
                    DirectCast(ctrl, CheckBox).Checked = True
                End If
            Next
        ElseIf (controsls = False) Then

            For Each ctrl As Control In Me.GroupBox2.Controls
                If TypeOf ctrl Is CheckBox Then
                    DirectCast(ctrl, CheckBox).Checked = False
                End If
            Next
        End If
    End Sub

    Private Sub docToPrint_EndPrint(sender As Object, e As PrintEventArgs) Handles docToPrint.EndPrint
        Me.Close()
    End Sub
End Class