﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("NT Semicoductor Transistor Tester")>
<Assembly: AssemblyDescription("Microcontroller 3 (Instrumentation) Transistor tester.")>
<Assembly: AssemblyCompany("Nepomuceno Technology Semiconductor")>
<Assembly: AssemblyProduct("NT Semicoductor Transistor Tester")>
<Assembly: AssemblyCopyright("Copyright © 2017 NTS A. Nepomuceno | F.Osorio")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("ae6cc5f6-fb6a-4ac9-acee-618c7673305b")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.1.0")>
<Assembly: AssemblyFileVersion("1.0.0.0")>
