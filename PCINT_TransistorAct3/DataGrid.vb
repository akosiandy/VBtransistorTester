﻿Imports System.Data.SQLite
Public Class DataGrid
    Dim adapter As New SQLiteDataAdapter
    Dim dataset As New DataTable
    Dim source As New BindingSource
    Public Sub New(Optional ByVal setgrid As String = "", Optional ByVal testcode As String = "")
        Dim datatorun As String
        Dim name As String
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
        Select Case setgrid
            Case "datadump"
                datadump()
            Case "showall"
                dynamicdatagrid("Select Trial,rb,rc,re,vc as VC,vre as VRE, vce as VCE,vb as VB,ib as IB,ic as IC,ie as IE,B as Beta,Testcode, Transistorname as 'Transistor in test' FROM transdata where testcode ='" & testcode & "'")
                Me.Text = "DataGrid ShowALL | Testcode: " & testcode
            Case "svo"
                dynamicdatagrid("Select Trial,rb,rc,re,vc as VC,vre as VRE,vce as VCE,vb as VB,B FROM transdata where testcode ='" & testcode & "'")
                Me.Text = "DataGrid Show Voltage Only | Testcode: " & testcode
            Case "sco"
                dynamicdatagrid("Select Trial,rb,rc,re,ib as IB,ic as IC,ie as IE,B FROM transdata where testcode ='" & testcode & "'")
                Me.Text = "DataGrid Show Current Only | Testcode: " & testcode
            Case "Beta"
                name = "Beta"
                datatorun = "B"
            Case "V|C"
                name = "Voltage In Collector"
                datatorun = "vc"
            Case "V|RE"
                name = "Voltage In Emitter"
                datatorun = "vre"
            Case "V|CE"
                name = "Voltage In Collector Emitter"
                datatorun = "vce"
            Case "V|RC"
                name = "Voltage In R Collector"
                datatorun = "vrc"
            Case "V|Base"
                name = "Voltage In Base"
                datatorun = "vb"
            Case "I|Base"
                name = "Current In Base"
                datatorun = "ib"
            Case "I|Collector"
                name = "Current In Collector"
                datatorun = "ic"
            Case "I|Emitter"
                name = "Current In Emitter"
                datatorun = "ie"
        End Select
        dynamicdatagrid("Select Trial,Transistorname as 'Transistor in Test'," & datatorun & " as '" & name & "',testcode FROM transdata where testcode ='" & testcode & "'")
        Me.Text = "DataGrid " & name & " | Testcode: " & testcode

    End Sub
    Private Sub dynamicdatagrid(ByVal querygraph As String)
        Dim mysqlcongraph = New SQLiteConnection
        mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
        Try
            mysqlcongraph.Open()
            'querygraph As String = "Select * FROM transdata "
            Dim Command = New SQLiteCommand(querygraph, mysqlcongraph)
            Dim reader = Command.ExecuteReader()

            Command = New SQLiteCommand(querygraph, mysqlcongraph)
            adapter.SelectCommand = Command
            adapter.Fill(dataset)
            source.DataSource = dataset
            DataGridView1.DataSource = source
            DataGridView1.AutoResizeColumns()
            While reader.Read
                Dim inc As Integer = inc + 1
                DataGridView1.Columns(reader.GetName(inc)).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End While
            adapter.Update(dataset)
            DataGridView1.FirstDisplayedScrollingRowIndex += 1
            mysqlcongraph.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        End Try

    End Sub

    Private Sub datadump()
        MsgBox("Warning All DATA UNDER DATA DUMP ARE RAW AND EDITABLE MUST ONLY BE USED FOR DEBUGGING NTS", MsgBoxStyle.Critical, "NTS SUPER WARNING")
        Me.Text = "DATA DUMP !"
        Dim mysqlcongraph = New SQLiteConnection
        mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
        Try
            mysqlcongraph.Open()
            Dim querygraph As String = "Select * FROM transdata "
            Dim Command = New SQLiteCommand(querygraph, mysqlcongraph)
            Dim reader = Command.ExecuteReader()

            Command = New SQLiteCommand(querygraph, mysqlcongraph)
            adapter.SelectCommand = Command
            adapter.Fill(dataset)
            source.DataSource = dataset
            DataGridView1.DataSource = source
            DataGridView1.AutoResizeColumns()
            While reader.Read
                Dim inc As Integer = inc + 1
                DataGridView1.Columns(reader.GetName(inc)).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End While
            adapter.Update(dataset)
            DataGridView1.FirstDisplayedScrollingRowIndex += 1
            mysqlcongraph.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub DataGrid_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class