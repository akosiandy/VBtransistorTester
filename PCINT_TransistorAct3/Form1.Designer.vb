﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.lblunitvc = New System.Windows.Forms.Label()
        Me.lblunitvre = New System.Windows.Forms.Label()
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lblvce = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtvce = New System.Windows.Forms.TextBox()
        Me.lblvrc = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtvrc = New System.Windows.Forms.TextBox()
        Me.lblvb = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtvb = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtib = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtic = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtie = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtb = New System.Windows.Forms.TextBox()
        Me.cbRB = New System.Windows.Forms.ComboBox()
        Me.cbRC = New System.Windows.Forms.ComboBox()
        Me.cbRE = New System.Windows.Forms.ComboBox()
        Me.btnstartform1 = New System.Windows.Forms.Button()
        Me.menu1 = New System.Windows.Forms.MenuStrip()
        Me.ConnectAndStartToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SetKGUToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.pb1 = New System.Windows.Forms.ToolStripProgressBar()
        Me.lblstatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.txttrial = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.gbParam = New System.Windows.Forms.GroupBox()
        Me.txtTestcode = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.gbComputed = New System.Windows.Forms.GroupBox()
        Me.gbMeasured = New System.Windows.Forms.GroupBox()
        Me.txtvcc = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.menu1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.gbParam.SuspendLayout()
        Me.gbComputed.SuspendLayout()
        Me.gbMeasured.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(30, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Rb ="
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 91)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(27, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Rc="
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(180, 69)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(30, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Re ="
        '
        'TextBox1
        '
        Me.TextBox1.Enabled = False
        Me.TextBox1.Location = New System.Drawing.Point(38, 18)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 20)
        Me.TextBox1.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 21)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(20, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Vc"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 47)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(23, 13)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Vre"
        '
        'TextBox2
        '
        Me.TextBox2.Enabled = False
        Me.TextBox2.Location = New System.Drawing.Point(38, 44)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(100, 20)
        Me.TextBox2.TabIndex = 7
        '
        'lblunitvc
        '
        Me.lblunitvc.AutoSize = True
        Me.lblunitvc.Location = New System.Drawing.Point(135, 19)
        Me.lblunitvc.Name = "lblunitvc"
        Me.lblunitvc.Size = New System.Drawing.Size(14, 13)
        Me.lblunitvc.TabIndex = 9
        Me.lblunitvc.Text = "V"
        '
        'lblunitvre
        '
        Me.lblunitvre.AutoSize = True
        Me.lblunitvre.Location = New System.Drawing.Point(135, 49)
        Me.lblunitvre.Name = "lblunitvre"
        Me.lblunitvre.Size = New System.Drawing.Size(14, 13)
        Me.lblunitvre.TabIndex = 10
        Me.lblunitvre.Text = "V"
        '
        'Timer1
        '
        Me.Timer1.Interval = 500
        '
        'lblvce
        '
        Me.lblvce.AutoSize = True
        Me.lblvce.Location = New System.Drawing.Point(142, 22)
        Me.lblvce.Name = "lblvce"
        Me.lblvce.Size = New System.Drawing.Size(14, 13)
        Me.lblvce.TabIndex = 16
        Me.lblvce.Text = "V"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(19, 20)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(26, 13)
        Me.Label11.TabIndex = 15
        Me.Label11.Text = "Vce"
        '
        'txtvce
        '
        Me.txtvce.Enabled = False
        Me.txtvce.Location = New System.Drawing.Point(45, 17)
        Me.txtvce.Name = "txtvce"
        Me.txtvce.Size = New System.Drawing.Size(100, 20)
        Me.txtvce.TabIndex = 14
        '
        'lblvrc
        '
        Me.lblvrc.AutoSize = True
        Me.lblvrc.Location = New System.Drawing.Point(142, 48)
        Me.lblvrc.Name = "lblvrc"
        Me.lblvrc.Size = New System.Drawing.Size(14, 13)
        Me.lblvrc.TabIndex = 20
        Me.lblvrc.Text = "V"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(19, 46)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(23, 13)
        Me.Label14.TabIndex = 19
        Me.Label14.Text = "Vrc"
        '
        'txtvrc
        '
        Me.txtvrc.Enabled = False
        Me.txtvrc.Location = New System.Drawing.Point(45, 43)
        Me.txtvrc.Name = "txtvrc"
        Me.txtvrc.Size = New System.Drawing.Size(100, 20)
        Me.txtvrc.TabIndex = 18
        '
        'lblvb
        '
        Me.lblvb.AutoSize = True
        Me.lblvb.Location = New System.Drawing.Point(137, 75)
        Me.lblvb.Name = "lblvb"
        Me.lblvb.Size = New System.Drawing.Size(14, 13)
        Me.lblvb.TabIndex = 23
        Me.lblvb.Text = "V"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(12, 73)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(20, 13)
        Me.Label16.TabIndex = 22
        Me.Label16.Text = "Vb"
        '
        'txtvb
        '
        Me.txtvb.Enabled = False
        Me.txtvb.Location = New System.Drawing.Point(38, 70)
        Me.txtvb.Name = "txtvb"
        Me.txtvb.Size = New System.Drawing.Size(100, 20)
        Me.txtvb.TabIndex = 21
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(142, 75)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(20, 13)
        Me.Label17.TabIndex = 26
        Me.Label17.Text = "uA"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(19, 73)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(16, 13)
        Me.Label18.TabIndex = 25
        Me.Label18.Text = "Ib"
        '
        'txtib
        '
        Me.txtib.Enabled = False
        Me.txtib.Location = New System.Drawing.Point(45, 70)
        Me.txtib.Name = "txtib"
        Me.txtib.Size = New System.Drawing.Size(100, 20)
        Me.txtib.TabIndex = 24
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(142, 101)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(22, 13)
        Me.Label19.TabIndex = 29
        Me.Label19.Text = "mA"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(19, 99)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(16, 13)
        Me.Label20.TabIndex = 28
        Me.Label20.Text = "Ic"
        '
        'txtic
        '
        Me.txtic.Enabled = False
        Me.txtic.Location = New System.Drawing.Point(45, 96)
        Me.txtic.Name = "txtic"
        Me.txtic.Size = New System.Drawing.Size(100, 20)
        Me.txtic.TabIndex = 27
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(142, 127)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(22, 13)
        Me.Label21.TabIndex = 32
        Me.Label21.Text = "mA"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(19, 125)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(16, 13)
        Me.Label22.TabIndex = 31
        Me.Label22.Text = "Ie"
        '
        'txtie
        '
        Me.txtie.Enabled = False
        Me.txtie.Location = New System.Drawing.Point(45, 122)
        Me.txtie.Name = "txtie"
        Me.txtie.Size = New System.Drawing.Size(100, 20)
        Me.txtie.TabIndex = 30
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(142, 153)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(0, 13)
        Me.Label23.TabIndex = 35
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(19, 151)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(14, 13)
        Me.Label24.TabIndex = 34
        Me.Label24.Text = "B"
        '
        'txtb
        '
        Me.txtb.Enabled = False
        Me.txtb.Location = New System.Drawing.Point(45, 148)
        Me.txtb.Name = "txtb"
        Me.txtb.Size = New System.Drawing.Size(100, 20)
        Me.txtb.TabIndex = 33
        '
        'cbRB
        '
        Me.cbRB.FormattingEnabled = True
        Me.cbRB.Items.AddRange(New Object() {"100k Ohms", "200k Ohms"})
        Me.cbRB.Location = New System.Drawing.Point(46, 66)
        Me.cbRB.Name = "cbRB"
        Me.cbRB.Size = New System.Drawing.Size(97, 21)
        Me.cbRB.TabIndex = 38
        Me.cbRB.Text = "100k Ohms"
        '
        'cbRC
        '
        Me.cbRC.FormattingEnabled = True
        Me.cbRC.Items.AddRange(New Object() {"500 Ohms", "1k Ohms"})
        Me.cbRC.Location = New System.Drawing.Point(45, 90)
        Me.cbRC.Name = "cbRC"
        Me.cbRC.Size = New System.Drawing.Size(97, 21)
        Me.cbRC.TabIndex = 39
        Me.cbRC.Text = "500 Ohms"
        '
        'cbRE
        '
        Me.cbRE.FormattingEnabled = True
        Me.cbRE.Items.AddRange(New Object() {"30 Ohms", "50 Ohms"})
        Me.cbRE.Location = New System.Drawing.Point(212, 66)
        Me.cbRE.Name = "cbRE"
        Me.cbRE.Size = New System.Drawing.Size(97, 21)
        Me.cbRE.TabIndex = 40
        Me.cbRE.Text = "50 Ohms"
        '
        'btnstartform1
        '
        Me.btnstartform1.Location = New System.Drawing.Point(183, 91)
        Me.btnstartform1.Name = "btnstartform1"
        Me.btnstartform1.Size = New System.Drawing.Size(126, 23)
        Me.btnstartform1.TabIndex = 41
        Me.btnstartform1.Text = "Start"
        Me.btnstartform1.UseVisualStyleBackColor = True
        '
        'menu1
        '
        Me.menu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConnectAndStartToolStripMenuItem, Me.SetKGUToolStripMenuItem1})
        Me.menu1.Location = New System.Drawing.Point(0, 0)
        Me.menu1.Name = "menu1"
        Me.menu1.Size = New System.Drawing.Size(388, 24)
        Me.menu1.TabIndex = 44
        Me.menu1.Text = "MenuStrip1"
        '
        'ConnectAndStartToolStripMenuItem
        '
        Me.ConnectAndStartToolStripMenuItem.Name = "ConnectAndStartToolStripMenuItem"
        Me.ConnectAndStartToolStripMenuItem.Size = New System.Drawing.Size(114, 20)
        Me.ConnectAndStartToolStripMenuItem.Text = "Connect and Start"
        '
        'SetKGUToolStripMenuItem1
        '
        Me.SetKGUToolStripMenuItem1.Name = "SetKGUToolStripMenuItem1"
        Me.SetKGUToolStripMenuItem1.Size = New System.Drawing.Size(61, 20)
        Me.SetKGUToolStripMenuItem1.Text = "Set KGU"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.pb1, Me.lblstatus})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 382)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(388, 22)
        Me.StatusStrip1.TabIndex = 45
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'pb1
        '
        Me.pb1.Name = "pb1"
        Me.pb1.Size = New System.Drawing.Size(100, 16)
        '
        'lblstatus
        '
        Me.lblstatus.Name = "lblstatus"
        Me.lblstatus.Size = New System.Drawing.Size(88, 17)
        Me.lblstatus.Text = "Not Connected"
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(21, 39)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox1.TabIndex = 46
        Me.ComboBox1.Text = "2n2222"
        '
        'txttrial
        '
        Me.txttrial.Location = New System.Drawing.Point(212, 39)
        Me.txttrial.Name = "txttrial"
        Me.txttrial.Size = New System.Drawing.Size(97, 20)
        Me.txttrial.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 23)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 13)
        Me.Label4.TabIndex = 48
        Me.Label4.Text = "Transistor:"
        '
        'gbParam
        '
        Me.gbParam.Controls.Add(Me.txtTestcode)
        Me.gbParam.Controls.Add(Me.Label8)
        Me.gbParam.Controls.Add(Me.Label5)
        Me.gbParam.Controls.Add(Me.Label1)
        Me.gbParam.Controls.Add(Me.Label2)
        Me.gbParam.Controls.Add(Me.txttrial)
        Me.gbParam.Controls.Add(Me.Label4)
        Me.gbParam.Controls.Add(Me.ComboBox1)
        Me.gbParam.Controls.Add(Me.Label3)
        Me.gbParam.Controls.Add(Me.cbRB)
        Me.gbParam.Controls.Add(Me.cbRC)
        Me.gbParam.Controls.Add(Me.cbRE)
        Me.gbParam.Controls.Add(Me.btnstartform1)
        Me.gbParam.Location = New System.Drawing.Point(18, 29)
        Me.gbParam.Name = "gbParam"
        Me.gbParam.Size = New System.Drawing.Size(347, 132)
        Me.gbParam.TabIndex = 49
        Me.gbParam.TabStop = False
        Me.gbParam.Text = "Parameters"
        '
        'txtTestcode
        '
        Me.txtTestcode.Location = New System.Drawing.Point(235, 16)
        Me.txtTestcode.Name = "txtTestcode"
        Me.txtTestcode.Size = New System.Drawing.Size(74, 20)
        Me.txtTestcode.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(180, 20)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(60, 13)
        Me.Label8.TabIndex = 52
        Me.Label8.Text = "BatchCode"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(180, 43)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(27, 13)
        Me.Label5.TabIndex = 52
        Me.Label5.Text = "Trial"
        '
        'gbComputed
        '
        Me.gbComputed.Controls.Add(Me.txtvce)
        Me.gbComputed.Controls.Add(Me.Label11)
        Me.gbComputed.Controls.Add(Me.lblvce)
        Me.gbComputed.Controls.Add(Me.txtvrc)
        Me.gbComputed.Controls.Add(Me.Label14)
        Me.gbComputed.Controls.Add(Me.lblvrc)
        Me.gbComputed.Controls.Add(Me.Label23)
        Me.gbComputed.Controls.Add(Me.Label24)
        Me.gbComputed.Controls.Add(Me.txtb)
        Me.gbComputed.Controls.Add(Me.Label21)
        Me.gbComputed.Controls.Add(Me.txtib)
        Me.gbComputed.Controls.Add(Me.Label22)
        Me.gbComputed.Controls.Add(Me.Label18)
        Me.gbComputed.Controls.Add(Me.txtie)
        Me.gbComputed.Controls.Add(Me.Label17)
        Me.gbComputed.Controls.Add(Me.Label19)
        Me.gbComputed.Controls.Add(Me.txtic)
        Me.gbComputed.Controls.Add(Me.Label20)
        Me.gbComputed.Location = New System.Drawing.Point(186, 171)
        Me.gbComputed.Name = "gbComputed"
        Me.gbComputed.Size = New System.Drawing.Size(171, 179)
        Me.gbComputed.TabIndex = 50
        Me.gbComputed.TabStop = False
        Me.gbComputed.Text = "Computed"
        '
        'gbMeasured
        '
        Me.gbMeasured.Controls.Add(Me.txtvcc)
        Me.gbMeasured.Controls.Add(Me.Label9)
        Me.gbMeasured.Controls.Add(Me.TextBox1)
        Me.gbMeasured.Controls.Add(Me.Label6)
        Me.gbMeasured.Controls.Add(Me.TextBox2)
        Me.gbMeasured.Controls.Add(Me.Label7)
        Me.gbMeasured.Controls.Add(Me.lblunitvc)
        Me.gbMeasured.Controls.Add(Me.txtvb)
        Me.gbMeasured.Controls.Add(Me.lblunitvre)
        Me.gbMeasured.Controls.Add(Me.Label16)
        Me.gbMeasured.Controls.Add(Me.lblvb)
        Me.gbMeasured.Location = New System.Drawing.Point(18, 167)
        Me.gbMeasured.Name = "gbMeasured"
        Me.gbMeasured.Size = New System.Drawing.Size(162, 121)
        Me.gbMeasured.TabIndex = 51
        Me.gbMeasured.TabStop = False
        Me.gbMeasured.Text = "Measured"
        '
        'txtvcc
        '
        Me.txtvcc.Enabled = False
        Me.txtvcc.Location = New System.Drawing.Point(38, 95)
        Me.txtvcc.Name = "txtvcc"
        Me.txtvcc.Size = New System.Drawing.Size(100, 20)
        Me.txtvcc.TabIndex = 13
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(12, 98)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(26, 13)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Vcc"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(388, 404)
        Me.Controls.Add(Me.gbMeasured)
        Me.Controls.Add(Me.gbComputed)
        Me.Controls.Add(Me.gbParam)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.menu1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.menu1
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.Text = "NTS - TESTER  v1"
        Me.menu1.ResumeLayout(False)
        Me.menu1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.gbParam.ResumeLayout(False)
        Me.gbParam.PerformLayout()
        Me.gbComputed.ResumeLayout(False)
        Me.gbComputed.PerformLayout()
        Me.gbMeasured.ResumeLayout(False)
        Me.gbMeasured.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents lblunitvc As Label
    Friend WithEvents lblunitvre As Label
    Friend WithEvents SerialPort1 As IO.Ports.SerialPort
    Friend WithEvents Timer1 As Timer
    Friend WithEvents lblvce As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents txtvce As TextBox
    Friend WithEvents lblvrc As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents txtvrc As TextBox
    Friend WithEvents lblvb As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents txtvb As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents txtib As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents txtic As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents txtie As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents txtb As TextBox
    Friend WithEvents cbRB As ComboBox
    Friend WithEvents cbRC As ComboBox
    Friend WithEvents cbRE As ComboBox
    Friend WithEvents btnstartform1 As Button
    Friend WithEvents menu1 As MenuStrip
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents lblstatus As ToolStripStatusLabel
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents txttrial As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents gbParam As GroupBox
    Friend WithEvents gbComputed As GroupBox
    Friend WithEvents gbMeasured As GroupBox
    Friend WithEvents Label5 As Label
    Friend WithEvents pb1 As ToolStripProgressBar
    Friend WithEvents txtvcc As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents ConnectAndStartToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SetKGUToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents txtTestcode As TextBox
    Friend WithEvents Label8 As Label
End Class
