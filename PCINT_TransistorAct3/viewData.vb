﻿Imports System.Data.SQLite
Public Class viewData
    Dim mysqlcon As SQLiteConnection
    Dim mysqlcongraph As SQLiteConnection
    Dim command As SQLiteCommand
    Dim dataset As New DataTable
    Dim source As New BindingSource
    Dim chartkgu As Integer
    Dim kgucount As Integer = 0
    Dim count
    Dim multiplied
    Dim units
    Dim datatorun As String
    Dim multipler
    Public Sub New(Optional ByVal datatorun As String = "")
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        Select Case datatorun
            Case "Beta"
                multipler = 1
                Name = "Beta"
                datatorun = "B"
                units = ""
            Case "V|C"
                multipler = 1
                Name = "Voltage In Collector"
                datatorun = "vc"
                units = "in Volts"
            Case "V|RE"
                multipler = 1000
                Name = "Voltage In Emitter"
                datatorun = "vre"
                units = "in milliVolts"
            Case "V|CE"
                multipler = 1000
                Name = "Voltage In Collector Emitter"
                datatorun = "vce"
                units = "in milliVolts"
            Case "V|RC"
                multipler = 1
                Name = "Voltage In R Collector"
                datatorun = "vrc"
                units = "in Volts"
            Case "V|Base"
                multipler = 1
                Name = "Voltage In Base"
                datatorun = "vb"
                units = "in Volts"
            Case "I|Base"
                multipler = 1000000
                Name = "Current In Base"
                datatorun = "ib"
                units = "in microAmps"
            Case "I|Collector"
                multipler = 1000
                Name = "Current In Collector"
                datatorun = "ic"
                units = "in milliAmps"
            Case "I|Emitter"
                multipler = 1000
                Name = "Current In Emitter"
                datatorun = "ie"
                units = "in milliAmps"
        End Select
        Me.Text = Name & " | " & datatorun & " - BATCHCODE = " & Mparent.Activetestcode & " | NTS Manual Graph System V 1"
        Chart1.Titles("TopName").Text = Name
        Chart1.ChartAreas(0).AxisY.Title = units
        getdata(datatorun)

    End Sub
    Private Sub getdata(ByVal query As String)
        mysqlcongraph = New SQLiteConnection
        mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
        Try
            mysqlcongraph.Open()
            Dim querygraph As String = "Select Trial , " & query & " FROM transdata Where iskgu = 'NO' AND Testcode='" & Mparent.Activetestcode & "'" ' Where Transistorname=" + Form1.kguintest
            command = New SQLiteCommand(querygraph, mysqlcongraph)
            Dim reader = command.ExecuteReader()


            While reader.Read
                multiplied = reader(query)
                multiplied = multiplied * multipler
                Chart1.Series(1).Points.AddXY(reader("Trial").ToString, multiplied)
                kgucount = kgucount + 1
            End While
            mysqlcongraph.Close()

            mysqlcongraph.Open()
            Dim querygraph1 As String = "SELECT min(" & query & ") , max(" & query & ") FROM transdata Where iskgu = 'NO' AND Testcode='" & Mparent.Activetestcode & "'"
            command = New SQLiteCommand(querygraph1, mysqlcongraph)
            reader = command.ExecuteReader
            Console.WriteLine(Mparent.Activetestcode)

            While reader.Read
                multiplied = reader("min(" & query & ")")
                multiplied = multiplied * multipler
                multiplied = Math.Round(multiplied, 0)
                Chart1.ChartAreas("data1").AxisY.Minimum = multiplied - 20
                Chart1.ChartAreas("data1").AxisY2.Minimum = multiplied - 20

                multiplied = reader("max(" & query & ")")
                multiplied = multiplied * multipler
                multiplied = Math.Round(multiplied, 0)
                Chart1.ChartAreas("data1").AxisY.Maximum = multiplied + 20
                Chart1.ChartAreas("data1").AxisY2.Maximum = multiplied + 20

            End While


            mysqlcongraph.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        End Try

        mysqlcongraph = New SQLiteConnection
        mysqlcongraph.ConnectionString = My.Resources.sqllogindetails
        Try
            mysqlcongraph.Open()
            Dim querygraph As String = "Select " & query & " FROM transdata Where iskgu = 'YES' AND Transistorname='" & Mparent.Activekgu & "'"
            Console.WriteLine(querygraph)
            command = New SQLiteCommand(querygraph, mysqlcongraph)
            Dim reader = command.ExecuteReader
            multiplied = 0
            While reader.Read
                multiplied = reader(query)
                multiplied = multiplied * multipler
                chartkgu = multiplied
            End While
            While (kgucount >= count And multiplied <> 0)
                Chart1.Series(0).Points.AddY(chartkgu.ToString)
                count = count + 1
            End While
            mysqlcongraph.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        End Try
    End Sub
End Class