﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class printdialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Title1 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(printdialog))
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CheckBox16 = New System.Windows.Forms.CheckBox()
        Me.b = New System.Windows.Forms.CheckBox()
        Me.ie = New System.Windows.Forms.CheckBox()
        Me.ic = New System.Windows.Forms.CheckBox()
        Me.ib = New System.Windows.Forms.CheckBox()
        Me.vb = New System.Windows.Forms.CheckBox()
        Me.vrc = New System.Windows.Forms.CheckBox()
        Me.vce = New System.Windows.Forms.CheckBox()
        Me.vre = New System.Windows.Forms.CheckBox()
        Me.nvc = New System.Windows.Forms.CheckBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CheckBox14 = New System.Windows.Forms.CheckBox()
        Me.CheckBox15 = New System.Windows.Forms.CheckBox()
        Me.CheckBox17 = New System.Windows.Forms.CheckBox()
        Me.CheckBox18 = New System.Windows.Forms.CheckBox()
        Me.CheckBox19 = New System.Windows.Forms.CheckBox()
        Me.CheckBox20 = New System.Windows.Forms.CheckBox()
        Me.CheckBox21 = New System.Windows.Forms.CheckBox()
        Me.CheckBox22 = New System.Windows.Forms.CheckBox()
        Me.CheckBox23 = New System.Windows.Forms.CheckBox()
        Me.CheckBox24 = New System.Windows.Forms.CheckBox()
        Me.CheckBox10 = New System.Windows.Forms.CheckBox()
        Me.CheckBox8 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Chart1 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(15, 25)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(293, 21)
        Me.ComboBox1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(138, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Select a BatchCode to Print"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CheckBox16)
        Me.GroupBox1.Controls.Add(Me.b)
        Me.GroupBox1.Controls.Add(Me.ie)
        Me.GroupBox1.Controls.Add(Me.ic)
        Me.GroupBox1.Controls.Add(Me.ib)
        Me.GroupBox1.Controls.Add(Me.vb)
        Me.GroupBox1.Controls.Add(Me.vrc)
        Me.GroupBox1.Controls.Add(Me.vce)
        Me.GroupBox1.Controls.Add(Me.vre)
        Me.GroupBox1.Controls.Add(Me.nvc)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 70)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(143, 128)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Include Charts"
        '
        'CheckBox16
        '
        Me.CheckBox16.AutoSize = True
        Me.CheckBox16.Location = New System.Drawing.Point(57, 105)
        Me.CheckBox16.Name = "CheckBox16"
        Me.CheckBox16.Size = New System.Drawing.Size(78, 17)
        Me.CheckBox16.TabIndex = 9
        Me.CheckBox16.Text = "Select ALL"
        Me.CheckBox16.UseVisualStyleBackColor = True
        '
        'b
        '
        Me.b.AutoSize = True
        Me.b.Location = New System.Drawing.Point(76, 65)
        Me.b.Name = "b"
        Me.b.Size = New System.Drawing.Size(33, 17)
        Me.b.TabIndex = 8
        Me.b.Text = "B"
        Me.b.UseVisualStyleBackColor = True
        '
        'ie
        '
        Me.ie.AutoSize = True
        Me.ie.Location = New System.Drawing.Point(76, 42)
        Me.ie.Name = "ie"
        Me.ie.Size = New System.Drawing.Size(36, 17)
        Me.ie.TabIndex = 7
        Me.ie.Text = "IE"
        Me.ie.UseVisualStyleBackColor = True
        '
        'ic
        '
        Me.ic.AutoSize = True
        Me.ic.Location = New System.Drawing.Point(76, 19)
        Me.ic.Name = "ic"
        Me.ic.Size = New System.Drawing.Size(36, 17)
        Me.ic.TabIndex = 6
        Me.ic.Text = "IC"
        Me.ic.UseVisualStyleBackColor = True
        '
        'ib
        '
        Me.ib.AutoSize = True
        Me.ib.Location = New System.Drawing.Point(3, 106)
        Me.ib.Name = "ib"
        Me.ib.Size = New System.Drawing.Size(36, 17)
        Me.ib.TabIndex = 5
        Me.ib.Text = "IB"
        Me.ib.UseVisualStyleBackColor = True
        '
        'vb
        '
        Me.vb.AutoSize = True
        Me.vb.Location = New System.Drawing.Point(76, 88)
        Me.vb.Name = "vb"
        Me.vb.Size = New System.Drawing.Size(40, 17)
        Me.vb.TabIndex = 4
        Me.vb.Text = "VB"
        Me.vb.UseVisualStyleBackColor = True
        '
        'vrc
        '
        Me.vrc.AutoSize = True
        Me.vrc.Location = New System.Drawing.Point(3, 83)
        Me.vrc.Name = "vrc"
        Me.vrc.Size = New System.Drawing.Size(48, 17)
        Me.vrc.TabIndex = 3
        Me.vrc.Text = "VRC"
        Me.vrc.UseVisualStyleBackColor = True
        '
        'vce
        '
        Me.vce.AutoSize = True
        Me.vce.Location = New System.Drawing.Point(3, 62)
        Me.vce.Name = "vce"
        Me.vce.Size = New System.Drawing.Size(47, 17)
        Me.vce.TabIndex = 2
        Me.vce.Text = "VCE"
        Me.vce.UseVisualStyleBackColor = True
        '
        'vre
        '
        Me.vre.AutoSize = True
        Me.vre.Location = New System.Drawing.Point(3, 39)
        Me.vre.Name = "vre"
        Me.vre.Size = New System.Drawing.Size(48, 17)
        Me.vre.TabIndex = 1
        Me.vre.Text = "VRE"
        Me.vre.UseVisualStyleBackColor = True
        '
        'nvc
        '
        Me.nvc.AutoSize = True
        Me.nvc.Location = New System.Drawing.Point(3, 16)
        Me.nvc.Name = "nvc"
        Me.nvc.Size = New System.Drawing.Size(40, 17)
        Me.nvc.TabIndex = 0
        Me.nvc.Text = "VC"
        Me.nvc.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CheckBox14)
        Me.GroupBox2.Controls.Add(Me.CheckBox15)
        Me.GroupBox2.Controls.Add(Me.CheckBox17)
        Me.GroupBox2.Controls.Add(Me.CheckBox18)
        Me.GroupBox2.Controls.Add(Me.CheckBox19)
        Me.GroupBox2.Controls.Add(Me.CheckBox20)
        Me.GroupBox2.Controls.Add(Me.CheckBox21)
        Me.GroupBox2.Controls.Add(Me.CheckBox22)
        Me.GroupBox2.Controls.Add(Me.CheckBox23)
        Me.GroupBox2.Controls.Add(Me.CheckBox24)
        Me.GroupBox2.Controls.Add(Me.CheckBox10)
        Me.GroupBox2.Controls.Add(Me.CheckBox8)
        Me.GroupBox2.Controls.Add(Me.CheckBox2)
        Me.GroupBox2.Location = New System.Drawing.Point(161, 71)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(152, 169)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "DATA"
        '
        'CheckBox14
        '
        Me.CheckBox14.AutoSize = True
        Me.CheckBox14.Location = New System.Drawing.Point(68, 127)
        Me.CheckBox14.Name = "CheckBox14"
        Me.CheckBox14.Size = New System.Drawing.Size(78, 17)
        Me.CheckBox14.TabIndex = 20
        Me.CheckBox14.Text = "Select ALL"
        Me.CheckBox14.UseVisualStyleBackColor = True
        '
        'CheckBox15
        '
        Me.CheckBox15.AutoSize = True
        Me.CheckBox15.Location = New System.Drawing.Point(69, 104)
        Me.CheckBox15.Name = "CheckBox15"
        Me.CheckBox15.Size = New System.Drawing.Size(33, 17)
        Me.CheckBox15.TabIndex = 19
        Me.CheckBox15.Text = "B"
        Me.CheckBox15.UseVisualStyleBackColor = True
        '
        'CheckBox17
        '
        Me.CheckBox17.AutoSize = True
        Me.CheckBox17.Location = New System.Drawing.Point(69, 81)
        Me.CheckBox17.Name = "CheckBox17"
        Me.CheckBox17.Size = New System.Drawing.Size(36, 17)
        Me.CheckBox17.TabIndex = 18
        Me.CheckBox17.Text = "IE"
        Me.CheckBox17.UseVisualStyleBackColor = True
        '
        'CheckBox18
        '
        Me.CheckBox18.AutoSize = True
        Me.CheckBox18.Location = New System.Drawing.Point(69, 58)
        Me.CheckBox18.Name = "CheckBox18"
        Me.CheckBox18.Size = New System.Drawing.Size(36, 17)
        Me.CheckBox18.TabIndex = 17
        Me.CheckBox18.Text = "IC"
        Me.CheckBox18.UseVisualStyleBackColor = True
        '
        'CheckBox19
        '
        Me.CheckBox19.AutoSize = True
        Me.CheckBox19.Location = New System.Drawing.Point(0, 150)
        Me.CheckBox19.Name = "CheckBox19"
        Me.CheckBox19.Size = New System.Drawing.Size(36, 17)
        Me.CheckBox19.TabIndex = 16
        Me.CheckBox19.Text = "IB"
        Me.CheckBox19.UseVisualStyleBackColor = True
        '
        'CheckBox20
        '
        Me.CheckBox20.AutoSize = True
        Me.CheckBox20.Location = New System.Drawing.Point(0, 127)
        Me.CheckBox20.Name = "CheckBox20"
        Me.CheckBox20.Size = New System.Drawing.Size(40, 17)
        Me.CheckBox20.TabIndex = 15
        Me.CheckBox20.Text = "VB"
        Me.CheckBox20.UseVisualStyleBackColor = True
        '
        'CheckBox21
        '
        Me.CheckBox21.AutoSize = True
        Me.CheckBox21.Location = New System.Drawing.Point(0, 104)
        Me.CheckBox21.Name = "CheckBox21"
        Me.CheckBox21.Size = New System.Drawing.Size(48, 17)
        Me.CheckBox21.TabIndex = 14
        Me.CheckBox21.Text = "VRC"
        Me.CheckBox21.UseVisualStyleBackColor = True
        '
        'CheckBox22
        '
        Me.CheckBox22.AutoSize = True
        Me.CheckBox22.Location = New System.Drawing.Point(0, 83)
        Me.CheckBox22.Name = "CheckBox22"
        Me.CheckBox22.Size = New System.Drawing.Size(47, 17)
        Me.CheckBox22.TabIndex = 13
        Me.CheckBox22.Text = "VCE"
        Me.CheckBox22.UseVisualStyleBackColor = True
        '
        'CheckBox23
        '
        Me.CheckBox23.AutoSize = True
        Me.CheckBox23.Location = New System.Drawing.Point(2, 60)
        Me.CheckBox23.Name = "CheckBox23"
        Me.CheckBox23.Size = New System.Drawing.Size(48, 17)
        Me.CheckBox23.TabIndex = 11
        Me.CheckBox23.Text = "VRE"
        Me.CheckBox23.UseVisualStyleBackColor = True
        '
        'CheckBox24
        '
        Me.CheckBox24.AutoSize = True
        Me.CheckBox24.Location = New System.Drawing.Point(2, 37)
        Me.CheckBox24.Name = "CheckBox24"
        Me.CheckBox24.Size = New System.Drawing.Size(40, 17)
        Me.CheckBox24.TabIndex = 10
        Me.CheckBox24.Text = "VC"
        Me.CheckBox24.UseVisualStyleBackColor = True
        '
        'CheckBox10
        '
        Me.CheckBox10.AutoSize = True
        Me.CheckBox10.Location = New System.Drawing.Point(69, 39)
        Me.CheckBox10.Name = "CheckBox10"
        Me.CheckBox10.Size = New System.Drawing.Size(41, 17)
        Me.CheckBox10.TabIndex = 2
        Me.CheckBox10.Text = "RE"
        Me.CheckBox10.UseVisualStyleBackColor = True
        '
        'CheckBox8
        '
        Me.CheckBox8.AutoSize = True
        Me.CheckBox8.Location = New System.Drawing.Point(69, 16)
        Me.CheckBox8.Name = "CheckBox8"
        Me.CheckBox8.Size = New System.Drawing.Size(41, 17)
        Me.CheckBox8.TabIndex = 1
        Me.CheckBox8.Text = "RC"
        Me.CheckBox8.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(3, 16)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(41, 17)
        Me.CheckBox2.TabIndex = 0
        Me.CheckBox2.Text = "RB"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(15, 249)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(921, 170)
        Me.DataGridView1.TabIndex = 4
        '
        'Chart1
        '
        Me.Chart1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Chart1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.LeftRight
        ChartArea1.AxisX.Minimum = 1.0R
        ChartArea1.AxisX.Title = "Number of Trials"
        ChartArea1.AxisX.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartArea1.AxisX.TitleForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        ChartArea1.AxisY.MaximumAutoSize = 100.0!
        ChartArea1.AxisY.Title = "Measurement"
        ChartArea1.AxisY.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartArea1.Name = "data1"
        Me.Chart1.ChartAreas.Add(ChartArea1)
        Legend1.Name = "Legend1"
        Me.Chart1.Legends.Add(Legend1)
        Me.Chart1.Location = New System.Drawing.Point(331, 26)
        Me.Chart1.Name = "Chart1"
        Series1.ChartArea = "data1"
        Series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
        Series1.Legend = "Legend1"
        Series1.MarkerBorderColor = System.Drawing.SystemColors.HotTrack
        Series1.MarkerSize = 8
        Series1.Name = "KGU"
        Series2.ChartArea = "data1"
        Series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point
        Series2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Series2.Label = "Trial #VALX\n #VAL{N2}"
        Series2.Legend = "Legend1"
        Series2.MarkerColor = System.Drawing.Color.DarkOrange
        Series2.MarkerSize = 3
        Series2.Name = "Trials"
        Me.Chart1.Series.Add(Series1)
        Me.Chart1.Series.Add(Series2)
        Me.Chart1.Size = New System.Drawing.Size(0, 0)
        Me.Chart1.TabIndex = 5
        Me.Chart1.Text = "Chart1"
        Title1.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Title1.Name = "TopName"
        Title1.Text = "hello"
        Me.Chart1.Titles.Add(Title1)
        Me.Chart1.Visible = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(12, 48)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Print"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.Visible = False
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(218, 49)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(90, 23)
        Me.Button2.TabIndex = 7
        Me.Button2.Text = "Print Preview"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'printdialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(319, 244)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Chart1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "printdialog"
        Me.Text = "PRINT DIALOG NTS"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents nvc As CheckBox
    Friend WithEvents ib As CheckBox
    Friend WithEvents vb As CheckBox
    Friend WithEvents vrc As CheckBox
    Friend WithEvents vce As CheckBox
    Friend WithEvents vre As CheckBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents CheckBox2 As CheckBox
    Friend WithEvents CheckBox16 As CheckBox
    Friend WithEvents b As CheckBox
    Friend WithEvents ie As CheckBox
    Friend WithEvents ic As CheckBox
    Friend WithEvents CheckBox14 As CheckBox
    Friend WithEvents CheckBox15 As CheckBox
    Friend WithEvents CheckBox17 As CheckBox
    Friend WithEvents CheckBox18 As CheckBox
    Friend WithEvents CheckBox19 As CheckBox
    Friend WithEvents CheckBox20 As CheckBox
    Friend WithEvents CheckBox21 As CheckBox
    Friend WithEvents CheckBox22 As CheckBox
    Friend WithEvents CheckBox23 As CheckBox
    Friend WithEvents CheckBox24 As CheckBox
    Friend WithEvents CheckBox10 As CheckBox
    Friend WithEvents CheckBox8 As CheckBox
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Chart1 As DataVisualization.Charting.Chart
    Friend WithEvents Button1 As Button
    Friend WithEvents PrintDialog1 As System.Windows.Forms.PrintDialog
    Friend WithEvents PrintPreviewDialog1 As PrintPreviewDialog
    Friend WithEvents Button2 As Button
End Class
